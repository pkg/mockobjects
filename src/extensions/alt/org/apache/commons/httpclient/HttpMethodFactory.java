package alt.org.apache.commons.httpclient;

import org.apache.commons.httpclient.methods.*;

public interface HttpMethodFactory {
    public PostMethod createPostMethod();
    public GetMethod createGetMethod();
    public PutMethod createPutMethod();
    public DeleteMethod createDeleteMethod();
    public HeadMethod createHeadMethod();
    public OptionsMethod createOptionsMethod();
}

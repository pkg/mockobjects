package alt.org.apache.commons.httpclient;

import org.apache.commons.httpclient.HttpClient;

public class HttpClientFactoryImpl implements HttpClientFactory{
    public HttpClient createHttpClient() {
        return new HttpClient();
    }
}

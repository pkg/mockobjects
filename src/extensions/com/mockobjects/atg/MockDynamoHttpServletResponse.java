// Decompiled by Jad v1.5.7. Copyright 1997-99 Pavel Kouznetsov.

// Jad home page: http://www.geocities.com/SiliconValley/Bridge/8617/jad.html

// Decompiler options: packimports(3)

// Source File Name:   MockDynamoHttpServletResponse.java



package com.mockobjects.atg;
import javax.servlet.http.HttpServletResponse;
import atg.servlet.*;
import com.mockobjects.*;
import java.io.IOException;public class MockDynamoHttpServletResponse extends DynamoHttpServletResponse implements Verifiable
{
    private ExpectationList myLocalRedirect = new ExpectationList("Local redirect");
    private boolean myShouldThrowExceptionOnRedirect = false;



    public MockDynamoHttpServletResponse() {
    }

    public MockDynamoHttpServletResponse(HttpServletResponse arg1) {
        super(arg1);
    }



    public void sendLocalRedirect(String url, DynamoHttpServletRequest request) throws IOException {
        myLocalRedirect.addActual(url);
        myLocalRedirect.addActual(request);
        if (myShouldThrowExceptionOnRedirect) {
            throw new IOException("Mock exception");
        }
    }



    public void setExpectedLocalRedirect(String url, DynamoHttpServletRequest request) {
        myLocalRedirect.addExpected(url);
        myLocalRedirect.addExpected(request);
    }



    public void setupThrowExceptionOnRedirect() {
        myShouldThrowExceptionOnRedirect = true;
    }



    public String toString() {
        return "MockDynamoServletResponse";
    }



    public void verify() {
        myLocalRedirect.verify();
    }
}
package com.mockobjects.apache.commons.httpclient;

import com.mockobjects.MockObject;
import com.mockobjects.ReturnObjectList;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.*;
import alt.org.apache.commons.httpclient.HttpMethodFactory;

public class MockHttpMethodFactory extends MockObject implements HttpMethodFactory {
    private final ReturnObjectList methods = new ReturnObjectList("methods");

    public void addCreateMethod(HttpMethod method) {
        methods.addObjectToReturn(method);
    }

    public PostMethod createPostMethod() {
        return (PostMethod) methods.nextReturnObject();
    }

    public PutMethod createPutMethod() {
        return (PutMethod) methods.nextReturnObject();
    }

    public GetMethod createGetMethod() {
        return (GetMethod) methods.nextReturnObject();
    }

    public DeleteMethod createDeleteMethod() {
        return (DeleteMethod) methods.nextReturnObject();
    }

    public HeadMethod createHeadMethod() {
        return (HeadMethod) methods.nextReturnObject();
    }

    public OptionsMethod createOptionsMethod() {
        return (OptionsMethod) methods.nextReturnObject();
    }

}

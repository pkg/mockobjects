package com.mockobjects.apache.commons.httpclient;

import com.mockobjects.*;
import com.mockobjects.util.*;
import org.apache.commons.httpclient.HttpState;

public class MockHttpState extends HttpState implements Verifiable {

    public void addCookie(org.apache.commons.httpclient.Cookie cookie){
	}

    public void addCookies(org.apache.commons.httpclient.Cookie[] cookie){
	}

    public org.apache.commons.httpclient.Cookie getCookies()[]{
        return null;
	}

    public org.apache.commons.httpclient.Cookie getCookies(String s1, int i1, String s2, boolean b1, java.util.Date d1)[]{
        return null;
	}

    public boolean purgeExpiredCookies(){
        return false;
	}

    public boolean purgeExpiredCookies(java.util.Date date){
        return false;
	}

    public void setCredentials(String string, org.apache.commons.httpclient.Credentials credentials){
	}

    public org.apache.commons.httpclient.Credentials getCredentials(String
    string){
        return null;
	}

    public void verify(){
        Verifier.verifyObject(this);
    }

}

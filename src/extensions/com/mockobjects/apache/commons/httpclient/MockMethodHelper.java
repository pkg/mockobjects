package com.mockobjects.apache.commons.httpclient;

import com.mockobjects.ReturnObjectBag;
import com.mockobjects.MockObject;
import org.apache.commons.httpclient.Header;

/**
 * @version $Revision: 1.1 $
 */
class MockMethodHelper extends MockObject{
    private final ReturnObjectBag header = new ReturnObjectBag("response header");

    public Header getResponseHeader(String key) {
        return (Header) header.getNextReturnObject(key);
    }

    public void addGetResponseHeader(String key, Header header) {
        this.header.putObjectToReturn(key, header);
    }
}

/*
 * Copyright (C) 2001 eZiba.com, Inc.
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.  Neither the name of eZiba.com nor the
 *    names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * eZiba.com OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package com.mockobjects.eziba.sql;class ArgumentArray
{

    ArgumentArray(Object [] p_args)
    {
        m_args = p_args;
    }

    public boolean equals(Object p_other)
    {
        if (! (p_other instanceof ArgumentArray) )
        {
            return false;
        }
        return arraysEqual(m_args, ((ArgumentArray)p_other).m_args);
    }

    public int hashCode()
    {
        return m_args.length;
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i = 0; i < m_args.length; ++i)
        {
            if (i > 0) { sb.append(','); }
            sb.append(m_args[i]);
        }
        sb.append(']');
        return sb.toString();
    }

    private static boolean objectsEqual(final Object p_first,
                                        final Object p_second)
    {
        if (p_first == null && p_second == null)
        {
            return true;
        }
        else if ( p_first == Wildcard.WILDCARD
                  || p_second == Wildcard.WILDCARD)
        {
            return true;
        }
        else if (p_first == null || p_second == null)
        {
            return false;
        }
        else
        {
            return p_first.equals(p_second);
        }
    }

    /**
     * compares two arrays; elements of an array match if both are
     * null, one is a wildcard, or they equal
     *@param p_first the first array
     *@param p_second the second array
     *@return boolean true if all elements match, false otherwise
     */

    private static boolean arraysEqual(final Object[] p_first,
                                       final Object[] p_second)
    {
        //if the number of elements differs, the arrays don't match by
        //definition.
        if ( p_first.length != p_second.length )
        {
            return false;
        }
        for (int i = 0; i < p_first.length; i++)
        {
            if ( ! objectsEqual(p_first[i],p_second[i]) )
            {
                return false;
            }
        }
        return true;
    }


    private final Object [] m_args;
}
package com.mockobjects.vaj;

import com.ibm.ivj.util.base.*;
import com.ibm.ivj.util.builders.*;
import java.util.*;
import com.mockobjects.*;

public class MockPackage extends MockObject implements com.ibm.ivj.util.base.Package {
    private String _pkgName;
    private ArrayList _types = new ArrayList();
    private MockWorkspace _workspace;
    private Project _project;

    public MockPackage(String name, MockWorkspace workspace) {
        super();
        _pkgName = name;
        _workspace = workspace;

        _workspace.setupAddPackage(this);
    }

    /**
     * @deprectated use setupAddContainedType
     */
    public void addContainedType(Type aType) {
        setupAddType(aType);
    }

    /**
     * @deprecated
     */
    public void addContainedType(MockType aType) {
        setupAddType(aType);
    }

    /**
     * clearToolRepositoryData method comment.
     */
    public void clearToolRepositoryData(String arg1) throws IvjException {}

    /**
     * clearToolWorkspaceData method comment.
     */
    public void clearToolWorkspaceData(String arg1) throws IvjException {}

    /**
     * createNewEdition method comment.
     */
    public void createNewEdition() throws IvjException {}

    /**
     * createVersion method comment.
     */
    public void createVersion(String arg1) throws IvjException {}

    /**
     * createVersion method comment.
     */
    public void createVersion(String arg1, boolean arg2) throws IvjException {}

    /**
     * delete method comment.
     */
    public void delete() throws IvjException {}

    /**
     * getAllEditions method comment.
     */
    public com.ibm.ivj.util.base.PackageEdition[] getAllEditions() throws IvjException {
        return null;
    }

    /**
     * getComment method comment.
     */
    public String getComment() throws IvjException {
        return null;
    }

    /**
     * getEdition method comment.
     */
    public PackageEdition getEdition() throws IvjException {
        return null;
    }

    /**
     * getName method comment.
     */
    public String getName() {
        return _pkgName;
    }

    /**
     * getOwnerName method comment.
     */
    public String getOwnerName() throws IvjException {
        return null;
    }

    public Project getProject() throws IvjException {
        return _project;
    }

    /**
     * getToolRepositoryData method comment.
     */
    public ToolData getToolRepositoryData(String arg1) throws java.io.IOException, java.io.StreamCorruptedException, IvjException, ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }

    /**
     * getToolWorkspaceData method comment.
     */
    public ToolData getToolWorkspaceData(String arg1) throws java.io.IOException, java.io.StreamCorruptedException, IvjException, ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }

    /**
     * getTypes method comment.
     */
    public com.ibm.ivj.util.base.Type[] getTypes() throws IvjException {
        Type[] result = new Type[_types.size()];
        _types.toArray(result);
        return result;
    }

    /**
     * getVersionName method comment.
     */
    public String getVersionName() throws IvjException {
        return null;
    }

    /**
     * getVersionStamp method comment.
     */
    public java.util.Date getVersionStamp() throws IvjException {
        return null;
    }

    /**
     * isDefaultPackage method comment.
     */
    public boolean isDefaultPackage() {
        return false;
    }

    /**
     * isEdition method comment.
     */
    public boolean isEdition() throws IvjException {
        return true;
    }

    /**
     * isPackage method comment.
     */
    public boolean isPackage() {
        return true;
    }

    /**
     * isProject method comment.
     */
    public boolean isProject() {
        return false;
    }

    /**
     * isReleased method comment.
     */
    public boolean isReleased() throws IvjException {
        return false;
    }

    /**
     * isScratchEdition method comment.
     */
    public boolean isScratchEdition() throws IvjException {
        return false;
    }

    /**
     * isType method comment.
     */
    public boolean isType() {
        return false;
    }

    /**
     * isVersion method comment.
     */
    public boolean isVersion() throws IvjException {
        return false;
    }

    /**
     * openBrowser method comment.
     */
    public void openBrowser() throws IvjException {}

    /**
     * release method comment.
     */
    public void release() throws IvjException {}

    /**
     * setComment method comment.
     */
    public void setComment(String arg1) throws IvjException {}

    /** @deprecated */

    public void setProject(Project project) {
        _project = project;
    }

    /**
     * setToolRepositoryData method comment.
     */
    public void setToolRepositoryData(ToolData arg1) throws java.io.IOException, IvjException {}

    /**
     * setToolWorkspaceData method comment.
     */
    public void setToolWorkspaceData(ToolData arg1) throws java.io.IOException, IvjException {}

    public void setupAddType(Type aType) {
        _types.add(aType);
        _workspace.setupAddType(aType);
    }

    public void setupAddType(MockType aType) {
        setupAddType((Type)aType);

        aType.setPackage(this);
    }

    public MockType setupNewType(String name) {
        MockType aType = new MockType(name);

        setupAddType(aType);

        return aType;
    }

    public void setupProject(Project project) {
        _project = project;
    }

    /**
     * testToolRepositoryData method comment.
     */
    public boolean testToolRepositoryData(String arg1) throws IvjException {
        return false;
    }

    /**
     * testToolWorkspaceData method comment.
     */
    public boolean testToolWorkspaceData(String arg1) throws IvjException {
        return false;
    }

    /**
     * verify method comment.
     */
    public void verify() {}
}

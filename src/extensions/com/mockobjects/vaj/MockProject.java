package com.mockobjects.vaj;

import com.ibm.ivj.util.base.*;
import com.mockobjects.*;

public class MockProject extends MockObject implements Project {

    /**
     * MockProject constructor comment.
     */
    public MockProject() {
        super();
    }

    /**
     * clearToolRepositoryData method comment.
     */
    public void clearToolRepositoryData(String arg1) throws IvjException {}

    /**
     * clearToolWorkspaceData method comment.
     */
    public void clearToolWorkspaceData(String arg1) throws IvjException {}

    /**
     * createDefaultPackage method comment.
     */
    public com.ibm.ivj.util.base.Package createDefaultPackage(boolean arg1) throws IvjException {
        return null;
    }

    /**
     * createNewEdition method comment.
     */
    public void createNewEdition() throws IvjException {}

    /**
     * createPackage method comment.
     */
    public com.ibm.ivj.util.base.Package createPackage(String arg1, boolean arg2) throws IvjException {
        return null;
    }

    /**
     * createVersion method comment.
     */
    public void createVersion(String arg1) throws IvjException {}

    /**
     * delete method comment.
     */
    public void delete() throws IvjException {}

    /**
     * getAllEditions method comment.
     */
    public com.ibm.ivj.util.base.ProjectEdition[] getAllEditions() throws IvjException {
        return null;
    }

    /**
     * getComment method comment.
     */
    public String getComment() throws IvjException {
        return null;
    }

    /**
     * getDefaultPackage method comment.
     */
    public com.ibm.ivj.util.base.Package getDefaultPackage() throws IvjException {
        return null;
    }

    /**
     * getEdition method comment.
     */
    public ProjectEdition getEdition() throws IvjException {
        return null;
    }

    /**
     * getName method comment.
     */
    public String getName() {
        return null;
    }

    /**
     * getOwnerName method comment.
     */
    public String getOwnerName() throws IvjException {
        return null;
    }

    /**
     * getPackages method comment.
     */
    public com.ibm.ivj.util.base.Package[] getPackages() throws IvjException {
        return null;
    }

    /**
     * getToolRepositoryData method comment.
     */
    public ToolData getToolRepositoryData(String arg1) throws java.io.IOException, java.io.StreamCorruptedException, IvjException, ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }

    /**
     * getToolWorkspaceData method comment.
     */
    public ToolData getToolWorkspaceData(String arg1) throws java.io.IOException, java.io.StreamCorruptedException, IvjException, ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }

    /**
     * getTypes method comment.
     */
    public com.ibm.ivj.util.base.Type[] getTypes() throws IvjException {
        return null;
    }

    /**
     * getVersionName method comment.
     */
    public String getVersionName() throws IvjException {
        return null;
    }

    /**
     * getVersionStamp method comment.
     */
    public java.util.Date getVersionStamp() throws IvjException {
        return null;
    }

    /**
     * isEdition method comment.
     */
    public boolean isEdition() throws IvjException {
        return false;
    }

    /**
     * isPackage method comment.
     */
    public boolean isPackage() {
        return false;
    }

    /**
     * isProject method comment.
     */
    public boolean isProject() {
        return false;
    }

    /**
     * isScratchEdition method comment.
     */
    public boolean isScratchEdition() throws IvjException {
        return false;
    }

    /**
     * isType method comment.
     */
    public boolean isType() {
        return false;
    }

    /**
     * isVersion method comment.
     */
    public boolean isVersion() throws IvjException {
        return false;
    }

    /**
     * openBrowser method comment.
     */
    public void openBrowser() throws IvjException {}

    /**
     * setComment method comment.
     */
    public void setComment(String arg1) throws IvjException {}

    /**
     * setToolRepositoryData method comment.
     */
    public void setToolRepositoryData(ToolData arg1) throws java.io.IOException, IvjException {}

    /**
     * setToolWorkspaceData method comment.
     */
    public void setToolWorkspaceData(ToolData arg1) throws java.io.IOException, IvjException {}

    /**
     * testToolRepositoryData method comment.
     */
    public boolean testToolRepositoryData(String arg1) throws IvjException {
        return false;
    }

    /**
     * testToolWorkspaceData method comment.
     */
    public boolean testToolWorkspaceData(String arg1) throws IvjException {
        return false;
    }

    /**
     * verify method comment.
     */
    public void verify() {}
}

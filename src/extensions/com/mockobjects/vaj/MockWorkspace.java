package com.mockobjects.vaj;

import java.util.*;
import java.io.File;

import com.ibm.ivj.util.base.*;
import com.ibm.ivj.util.builders.*;
import com.ibm.ivj.util.base.Package;

import junit.framework.*;

import com.mockobjects.*;
import com.mockobjects.util.*;

public class MockWorkspace extends MockObject implements Workspace {
    public final static String DEFAULT_PKG_PREFIX = "Default package for ";

    private MockBuilderFactory myBuilderFactory;
    private ArrayList myLoadedTypes = new ArrayList();
    private ArrayList myLoadedPackages = new ArrayList();
    private SourceVerifier mySourceVerifier;
    private ExportCodeSpec myExpectedExport;
    private ExportCodeSpec myActualExport;
    private ImportCodeSpec myExpectedImport;
    private ImportCodeSpec myActualImport;
    private HashMap myImportableFiles = new HashMap();


    public MockWorkspace() {
        this(new SourceVerifier());
    }


    public MockWorkspace(SourceVerifier verifier) {
        super();

        mySourceVerifier = verifier;
        myBuilderFactory = new MockBuilderFactory(verifier);
    }

    /**
     * @deprecated use setupAddImportableFile
     */

    public void addImportableFile(String fileName, Type associatedType) {
        setupAddImportableFile(fileName, associatedType);
    }

    /**
     * @deprecated use setupAddPackage
     */

    public void addLoadedPackage(Package pkg) {
        setupAddPackage(pkg);
    }

    /**
     * @deprecated use setupAddType
     */
    public void addLoadedType(Type type) {
        setupAddType(type);
    }

    /**
     * changeOwner method comment.
     */
    public void changeOwner(java.lang.String arg1, java.lang.String arg2) throws com.ibm.ivj.util.base.IvjException {}

    /**
     * clearToolOptions method comment.
     */
    public void clearToolOptions(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {}

    public BuilderFactory createBuilderFactory() {
        return myBuilderFactory;
    }

    /**
     * createProject method comment.
     */
    public com.ibm.ivj.util.base.Project createProject(java.lang.String arg1, boolean arg2) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    public void exportData(ExportCodeSpec spec) throws IvjException {
        AssertMo.assertNull(myActualExport);
        myActualExport = spec;

        verifyExportSpec();
    }

    /**
     * exportData method comment.
     */
    public void exportData(com.ibm.ivj.util.base.ExportInterchangeSpec arg1) throws com.ibm.ivj.util.base.IvjException {}

    /**
     * getClassPathEntries method comment.
     */
    public java.lang.Object[] getClassPathEntries() {
        return null;
    }

    /**
     * getCurrentOwnerName method comment.
     */
    public java.lang.String getCurrentOwnerName() {
        return null;
    }

    /**
     * getDefaultClassPathEntries method comment.
     */
    public java.lang.Object[] getDefaultClassPathEntries() {
        return null;
    }

    public static String getDefaultPackageFor(String projectName) {
        return DEFAULT_PKG_PREFIX + projectName;
    }

    /**
     * getPackages method comment.
     */
    public com.ibm.ivj.util.base.Package[] getPackages() {
        return null;
    }

    /**
     * getProjects method comment.
     */
    public com.ibm.ivj.util.base.Project[] getProjects() {
        return null;
    }

    /**
     * getRepository method comment.
     */
    public com.ibm.ivj.util.base.Repository getRepository() {
        return null;
    }

    /**
     * getRepository method comment.
     */
    public com.ibm.ivj.util.base.Repository getRepository(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getRepository method comment.
     */
    public com.ibm.ivj.util.base.Repository getRepository(java.lang.String arg1, java.lang.String arg2) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getTempDirectory method comment.
     */
    public java.lang.String getTempDirectory() throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getToolDataDirectory method comment.
     */
    public java.lang.String getToolDataDirectory(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getToolOptions method comment.
     */
    public com.ibm.ivj.util.base.ToolData getToolOptions(java.lang.String arg1) throws java.io.IOException, java.io.StreamCorruptedException, com.ibm.ivj.util.base.IvjException, java.lang.ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }

    /**
     * getTypes method comment.
     */
    public com.ibm.ivj.util.base.Type[] getTypes() {
        Type[] result = new Type[myLoadedTypes.size()];
        myLoadedTypes.toArray(result);
        return result;
    }

    /**
     * getValidUsers method comment.
     */
    public java.lang.String[] getValidUsers() {
        return null;
    }

    public Type[] importData(ImportCodeSpec importer) throws IllegalArgumentException, IvjException {
        AssertMo.assertNull(myActualImport);
        myActualImport = importer;

        verifyImportSpec();
        return matchImportedTypes(importer);
    }

    /**
     * importData method comment.
     */
    public void importData(com.ibm.ivj.util.base.ImportInterchangeSpec arg1) throws com.ibm.ivj.util.base.IvjException {}

    /**
     * isTeamMode method comment.
     */
    public boolean isTeamMode() {
        return false;
    }

    /**
     * loadedDefaultPackageFor method comment.
     */
    public com.ibm.ivj.util.base.Package loadedDefaultPackageFor(String projectName) {
        return loadedPackageNamed(DEFAULT_PKG_PREFIX + projectName);
    }

    /**
     * loadedDefaultPackagesFor method comment.
     */
    public com.ibm.ivj.util.base.Package[] loadedDefaultPackagesFor(java.lang.String[] arg1) {
        return null;
    }

    /**
     * loadedPackageNamed method comment.
     */
    public com.ibm.ivj.util.base.Package loadedPackageNamed(String pkgName) {

        for(Iterator it = myLoadedPackages.iterator(); it.hasNext(); ) {
            Package aPkg = (Package)it.next();
            if(aPkg.getName().equals(pkgName))
                return aPkg;
        }

        return null;
    }

    /**
     * loadedPackagesNamed method comment.
     */
    public com.ibm.ivj.util.base.Package[] loadedPackagesNamed(java.lang.String[] arg1) {
        return null;
    }

    /**
     * loadedProjectNamed method comment.
     */
    public com.ibm.ivj.util.base.Project loadedProjectNamed(java.lang.String arg1) {
        return null;
    }

    /**
     * loadedProjectsNamed method comment.
     */
    public com.ibm.ivj.util.base.Project[] loadedProjectsNamed(java.lang.String[] arg1) {
        return null;
    }

    public Type loadedTypeNamed(String typeName) {
        for(Iterator it = myLoadedTypes.iterator(); it.hasNext(); ) {
            Type aType = (Type)it.next();
            if(aType.getQualifiedName().equals(typeName))
                return aType;
        }

        return null;
    }

    public Type[] loadedTypesNamed(String[] typeNames) {
        if (null == typeNames) {
            return null;
        }

        ArrayList loaded = new ArrayList();

        for (int i = 0; i < typeNames.length; ++i) {
            loaded.add(loadedTypeNamed(typeNames[i]));
        }
        Type[] result = null;
        if (!loaded.isEmpty()) {
            result = new Type[loaded.size()];
            loaded.toArray(result);
        }
        return result;
    }

    /**
     * logMessage method comment.
     */
    public void logMessage(java.lang.String arg1, boolean arg2) {}

    private Type[] matchImportedTypes(ImportCodeSpec importer) throws IllegalArgumentException, IvjException {
        ArrayList types = new ArrayList();
        String[] files = importer.getJavaFiles();
        for (int i = 0; i < files.length; ++i) {
            Object type = myImportableFiles.get(files[i]);
            if (null != type) {
                types.add(type);
            }
        }
        Type[] result = null;
        if (!types.isEmpty()) {
            result = new Type[types.size()];
            types.toArray(result);
        }
        return result;
    }

    /**
     * promptForApplet method comment.
     */
    public com.ibm.ivj.util.base.Type promptForApplet(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForApplication method comment.
     */
    public com.ibm.ivj.util.base.Type promptForApplication(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForClass method comment.
     */
    public com.ibm.ivj.util.base.Type promptForClass(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForDirectoryName method comment.
     */
    public java.lang.String promptForDirectoryName(java.lang.String arg1, java.lang.String arg2) {
        return null;
    }

    /**
     * promptForFileName method comment.
     */
    public java.lang.String promptForFileName(java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) {
        return null;
    }

    /**
     * promptForInterface method comment.
     */
    public com.ibm.ivj.util.base.Type promptForInterface(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForModel method comment.
     */
    public com.ibm.ivj.util.base.WorkspaceModel promptForModel(com.ibm.ivj.util.base.WorkspaceModel[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForPackage method comment.
     */
    public com.ibm.ivj.util.base.Package promptForPackage(com.ibm.ivj.util.base.Package[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForPackage method comment.
     */
    public com.ibm.ivj.util.base.Package promptForPackage(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.Project arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForProject method comment.
     */
    public com.ibm.ivj.util.base.Project promptForProject(com.ibm.ivj.util.base.Project[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForProject method comment.
     */
    public com.ibm.ivj.util.base.Project promptForProject(java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) {
        return null;
    }

    /**
     * promptForType method comment.
     */
    public com.ibm.ivj.util.base.Type promptForType(com.ibm.ivj.util.base.Type[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * promptForType method comment.
     */
    public com.ibm.ivj.util.base.Type promptForType(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * removeClassPathEntries method comment.
     */
    public void removeClassPathEntries(java.lang.Object[] arg1) throws com.ibm.ivj.util.base.IvjException {}

    /**
     * runMain method comment.
     */
    public void runMain(java.lang.Class arg1, java.lang.String[] arg2) throws java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.IllegalArgumentException, java.lang.NoSuchMethodException, com.ibm.ivj.util.base.IvjException {}

    /**
     * setClassPath method comment.
     */
    public void setClassPath(java.lang.Object[] arg1) throws com.ibm.ivj.util.base.IvjException {}

    public void setExpectedExport(ExportCodeSpec export) {
        myExpectedExport = export;
    }

    public void setExpectedImport(ImportCodeSpec importer) {
        myExpectedImport = importer;
    }

    /**
     * setToolOptions method comment.
     */
    public void setToolOptions(com.ibm.ivj.util.base.ToolData arg1) throws java.io.IOException, com.ibm.ivj.util.base.IvjException {}

    /**
     * Mock method
     */

    public void setupAddImportableFile(String fileName, Type associatedType) {
        myImportableFiles.put(fileName, associatedType);
    }

    /**
     * Mock method
     */

    public void setupAddPackage(Package pkg) {
        myLoadedPackages.add(pkg);
    }

    public void setupAddType(Type type) {
        myLoadedTypes.add(type);
    }

    /**
     * Mock method
     */

    public MockPackage setupNewDefaltPackage(String projectName) {
        return new MockPackage(getDefaultPackageFor(projectName), this);
    }

    /**
     * Mock method
     */

    public MockPackage setupNewPackage(String pkgName) {
        return new MockPackage(pkgName, this);
    }

    /**
     * shutdown method comment.
     */
    public void shutdown() {}

    /**
     * testToolOptions method comment.
     */
    public boolean testToolOptions(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {
        return false;
    }

    /**
     * Mock method
     */

    public void verify() {
        mySourceVerifier.verify();
        myBuilderFactory.verify();
    }

    /**
     * Mock method
     */

    private void verifyExportSpec() {
        if (null == myExpectedExport) {
            return;
        }
        AssertMo.assertNotNull("Should set export spec", myActualExport);

        AssertMo.assert("Use subdirectories", myExpectedExport.useSubdirectories() == myActualExport.useSubdirectories());
        AssertMo.assert("include class", myExpectedExport.includeClass() == myActualExport.includeClass());
        AssertMo.assertEquals("directory", myExpectedExport.getExportDirectory(), myActualExport.getExportDirectory());

        ExpectationSet types = new ExpectationSet("Matching types");
        types.addActualMany(myActualExport.getTypes());
        types.addExpectedMany(myExpectedExport.getTypes());
        types.verify();
    }

    /**
     * Mock method
     */

    private void verifyImportSpec() {
        if (null == myExpectedImport) {
            return;
        }
        AssertMo.assertNotNull("Should set export spec", myActualImport);
        AssertMo.assertEquals("Verify default project",
            myExpectedImport.getDefaultProject(),
            myActualImport.getDefaultProject());

        ExpectationSet javaFiles = new ExpectationSet("Matching import types");
        javaFiles.addActualMany(myActualImport.getJavaFiles());
        javaFiles.addExpectedMany(myExpectedImport.getJavaFiles());
        javaFiles.verify();
    }
}

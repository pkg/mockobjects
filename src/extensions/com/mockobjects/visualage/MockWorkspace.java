package com.mockobjects.visualage;
import com.ibm.ivj.util.base.*;
import com.ibm.ivj.util.builders.*;
import com.ibm.ivj.util.base.Package;
import junit.framework.*;
import java.util.*;
import com.mockobjects.Verifiable;public class MockWorkspace implements Workspace, Verifiable {
    private MockBuilderFactory myBuilderFactory;
    private ArrayList myLoadedTypes = new ArrayList();
    private ArrayList myLoadedPackages = new ArrayList();
    private SourceVerifier mySourceVerifier;



    public MockWorkspace() {
    }



    public MockWorkspace(MockType aType) {
        myBuilderFactory = new MockBuilderFactory(new MockTypeBuilder(aType));
    }



    public MockWorkspace(SourceVerifier verifier) {
        this(verifier, new MockBuilderFactory(verifier));
    }



    public MockWorkspace(SourceVerifier verifier, MockBuilderFactory builderFactory) {
        super();
        mySourceVerifier = verifier;
        myBuilderFactory = builderFactory;
    }



    /**
     * Mock method
     */

    public void addLoadedPackage(Package pkg) {
        myLoadedPackages.add(pkg);
    }



    /**
     * Mock method
     */
    public void addLoadedType(Type type) {
        myLoadedTypes.add(type);
    }



    /**
     * changeOwner method comment.
     */
    public void changeOwner(java.lang.String arg1, java.lang.String arg2) throws com.ibm.ivj.util.base.IvjException {}



    /**
     * clearToolOptions method comment.
     */
    public void clearToolOptions(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {}



    /**
     * createBuilderFactory method comment.
     */
    public com.ibm.ivj.util.builders.BuilderFactory createBuilderFactory() {
        return myBuilderFactory;
    }



    /**
     * createProject method comment.
     */
    public com.ibm.ivj.util.base.Project createProject(java.lang.String arg1, boolean arg2) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * exportData method comment.
     */
    public void exportData(com.ibm.ivj.util.base.ExportCodeSpec arg1) throws com.ibm.ivj.util.base.IvjException {}



    /**
     * exportData method comment.
     */
    public void exportData(com.ibm.ivj.util.base.ExportInterchangeSpec arg1) throws com.ibm.ivj.util.base.IvjException {}



    /**
     * getClassPathEntries method comment.
     */
    public java.lang.Object[] getClassPathEntries() {
        return null;
    }



    /**
     * getCurrentOwnerName method comment.
     */
    public java.lang.String getCurrentOwnerName() {
        return null;
    }



    /**
     * getDefaultClassPathEntries method comment.
     */
    public java.lang.Object[] getDefaultClassPathEntries() {
        return null;
    }



    /**
     * getPackages method comment.
     */
    public com.ibm.ivj.util.base.Package[] getPackages() {
        return null;
    }



    /**
     * getProjects method comment.
     */
    public com.ibm.ivj.util.base.Project[] getProjects() {
        return null;
    }



    /**
     * getRepository method comment.
     */
    public com.ibm.ivj.util.base.Repository getRepository() {
        return null;
    }



    /**
     * getRepository method comment.
     */
    public com.ibm.ivj.util.base.Repository getRepository(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * getRepository method comment.
     */
    public com.ibm.ivj.util.base.Repository getRepository(java.lang.String arg1, java.lang.String arg2) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * getTempDirectory method comment.
     */
    public java.lang.String getTempDirectory() throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * getToolDataDirectory method comment.
     */
    public java.lang.String getToolDataDirectory(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * getToolOptions method comment.
     */
    public com.ibm.ivj.util.base.ToolData getToolOptions(java.lang.String arg1) throws java.io.IOException, java.io.StreamCorruptedException, com.ibm.ivj.util.base.IvjException, java.lang.ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }



    /**
     * getTypes method comment.
     */
    public com.ibm.ivj.util.base.Type[] getTypes() {
        Type[] result = new Type[myLoadedTypes.size()];
        myLoadedTypes.toArray(result);
        return result;
    }



    /**
     * getValidUsers method comment.
     */
    public java.lang.String[] getValidUsers() {
        return null;
    }



    /**
     * importData method comment.
     */
    public com.ibm.ivj.util.base.Type[] importData(com.ibm.ivj.util.base.ImportCodeSpec arg1) throws java.lang.IllegalArgumentException, com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * importData method comment.
     */
    public void importData(com.ibm.ivj.util.base.ImportInterchangeSpec arg1) throws com.ibm.ivj.util.base.IvjException {}



    /**
     * isTeamMode method comment.
     */
    public boolean isTeamMode() {
        return false;
    }



    /**
     * loadedDefaultPackageFor method comment.
     */
    public com.ibm.ivj.util.base.Package loadedDefaultPackageFor(java.lang.String arg1) {
        return null;
    }



    /**
     * loadedDefaultPackagesFor method comment.
     */
    public com.ibm.ivj.util.base.Package[] loadedDefaultPackagesFor(java.lang.String[] arg1) {
        return null;
    }



    /**
     * loadedPackageNamed method comment.
     */
    public com.ibm.ivj.util.base.Package loadedPackageNamed(String pkgName) {

        for(Iterator it = myLoadedPackages.iterator(); it.hasNext(); ) {
            Package aPkg = (Package)it.next();
            if(aPkg.getName().equals(pkgName))
                return aPkg;
        }

        return null;
    }



    /**
     * loadedPackagesNamed method comment.
     */
    public com.ibm.ivj.util.base.Package[] loadedPackagesNamed(java.lang.String[] arg1) {
        return null;
    }



    /**
     * loadedProjectNamed method comment.
     */
    public com.ibm.ivj.util.base.Project loadedProjectNamed(java.lang.String arg1) {
        return null;
    }



    /**
     * loadedProjectsNamed method comment.
     */
    public com.ibm.ivj.util.base.Project[] loadedProjectsNamed(java.lang.String[] arg1) {
        return null;
    }



    /**
     * loadedTypeNamed method comment.
     */
    public com.ibm.ivj.util.base.Type loadedTypeNamed(java.lang.String typeName) {

        for(Iterator it = myLoadedTypes.iterator(); it.hasNext(); ) {
            Type aType = (Type)it.next();
            if(aType.getQualifiedName().equals(typeName))
                return aType;
        }

        return null;
    }



    /**
     * loadedTypesNamed method comment.
     */
    public com.ibm.ivj.util.base.Type[] loadedTypesNamed(java.lang.String[] arg1) {
        return null;
    }



    /**
     * logMessage method comment.
     */
    public void logMessage(java.lang.String arg1, boolean arg2) {}



    /**
     * promptForApplet method comment.
     */
    public com.ibm.ivj.util.base.Type promptForApplet(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForApplication method comment.
     */
    public com.ibm.ivj.util.base.Type promptForApplication(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForClass method comment.
     */
    public com.ibm.ivj.util.base.Type promptForClass(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForDirectoryName method comment.
     */
    public java.lang.String promptForDirectoryName(java.lang.String arg1, java.lang.String arg2) {
        return null;
    }



    /**
     * promptForFileName method comment.
     */
    public java.lang.String promptForFileName(java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) {
        return null;
    }



    /**
     * promptForInterface method comment.
     */
    public com.ibm.ivj.util.base.Type promptForInterface(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForModel method comment.
     */
    public com.ibm.ivj.util.base.WorkspaceModel promptForModel(com.ibm.ivj.util.base.WorkspaceModel[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForPackage method comment.
     */
    public com.ibm.ivj.util.base.Package promptForPackage(com.ibm.ivj.util.base.Package[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForPackage method comment.
     */
    public com.ibm.ivj.util.base.Package promptForPackage(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.Project arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForProject method comment.
     */
    public com.ibm.ivj.util.base.Project promptForProject(com.ibm.ivj.util.base.Project[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForProject method comment.
     */
    public com.ibm.ivj.util.base.Project promptForProject(java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) {
        return null;
    }



    /**
     * promptForType method comment.
     */
    public com.ibm.ivj.util.base.Type promptForType(com.ibm.ivj.util.base.Type[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * promptForType method comment.
     */
    public com.ibm.ivj.util.base.Type promptForType(java.lang.String arg1, java.lang.String arg2, com.ibm.ivj.util.base.WorkspaceModel arg3, java.lang.String arg4) throws com.ibm.ivj.util.base.IvjException {
        return null;
    }



    /**
     * removeClassPathEntries method comment.
     */
    public void removeClassPathEntries(java.lang.Object[] arg1) throws com.ibm.ivj.util.base.IvjException {}



    /**
     * runMain method comment.
     */
    public void runMain(java.lang.Class arg1, java.lang.String[] arg2) throws java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.IllegalArgumentException, java.lang.NoSuchMethodException, com.ibm.ivj.util.base.IvjException {}



    /**
     * setClassPath method comment.
     */
    public void setClassPath(java.lang.Object[] arg1) throws com.ibm.ivj.util.base.IvjException {}



    /**
     * setToolOptions method comment.
     */
    public void setToolOptions(com.ibm.ivj.util.base.ToolData arg1) throws java.io.IOException, com.ibm.ivj.util.base.IvjException {}



    /**
     * shutdown method comment.
     */
    public void shutdown() {}



    /**
     * testToolOptions method comment.
     */
    public boolean testToolOptions(java.lang.String arg1) throws com.ibm.ivj.util.base.IvjException {
        return false;
    }



    public void verify() {
        mySourceVerifier.verify();
        myBuilderFactory.verify();
    }
}
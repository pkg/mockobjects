package com.mockobjects.visualage;
import java.util.*;
import com.ibm.ivj.util.builders.BuilderFactory;
import com.ibm.ivj.util.builders.MethodBuilder;
import com.ibm.ivj.util.builders.TypeBuilder;
import com.ibm.ivj.util.base.Package;
import com.ibm.ivj.util.base.Type;
import com.ibm.ivj.util.base.IvjException;public class MockBuilderFactory implements BuilderFactory {
    private ArrayList myTypeBuilders = new ArrayList();
    private SourceVerifier mySourceVerifier;
    private MockTypeBuilder mySingleTypeBuilder;



    public MockBuilderFactory(MockTypeBuilder builder) {
        mySingleTypeBuilder = builder;
    }



    public MockBuilderFactory(SourceVerifier verifier) {
        super();
        mySourceVerifier = verifier;
    }



    public MethodBuilder createMethodBuilder(String builderName) throws java.lang.IllegalArgumentException {
        return new MockMethodBuilder(mySourceVerifier,builderName);
    }



    public TypeBuilder createTypeBuilder(Type aType) throws IllegalArgumentException, IvjException {
        return mySingleTypeBuilder;
    }



    public TypeBuilder createTypeBuilder(String typeName, Package pkg) throws IllegalArgumentException, IvjException {
        MockTypeBuilder builder = new MockTypeBuilder(mySourceVerifier,typeName);
        myTypeBuilders.add(builder);
        return builder;
    }



    public String createUserCodeBlock(String arg1, String arg2) {
        return null;
    }



    public void verify() {
        for(Iterator it = myTypeBuilders.iterator();it.hasNext();) {
            ((MockTypeBuilder)it.next()).verify();
        }
    }
}
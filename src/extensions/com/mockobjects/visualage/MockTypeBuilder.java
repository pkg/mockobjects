package com.mockobjects.visualage;
import java.util.*;
import com.ibm.ivj.util.base.*;
import com.ibm.ivj.util.builders.*;
import junit.framework.*;
import com.mockobjects.ExpectationCounter;
import com.mockobjects.Verifiable;public class MockTypeBuilder implements TypeBuilder, Verifiable {
    private String myBuilderName;
    private SourceVerifier mySourceVerifier;
    private ExpectationCounter mySaveCounter;
    private MockType myType;

    public MockTypeBuilder(MockType aType) {
        myType = aType;
    }

    public MockTypeBuilder(SourceVerifier verifier, String builderName) {
        super();

        mySourceVerifier = verifier;
        myBuilderName = builderName;
        mySaveCounter = new ExpectationCounter(builderName + ".save()");
        mySaveCounter.setExpected(1);
    }

    /**
     * addMethodBuilder method comment.
     */
    public void addMethodBuilder(MethodBuilder arg1) {
    }

    /**
     * checkMethodExists method comment.
     */
    public boolean checkMethodExists(MethodBuilder arg1) throws IvjException {
        return false;
    }

    /**
     * getBuilderName method comment.
     */
    public String getBuilderName() {
        return myBuilderName;
    }

    /**
     * getExistingMethods method comment.
     */
    public com.ibm.ivj.util.builders.MethodBuilder[] getExistingMethods()
        throws IvjException {
        return null;
    }

    public String getExistingMethodSource(MethodBuilder builder)
        throws IvjException {
        return ((MockMethodBuilder) builder).myExistingSource;
    }

    public String getExistingSource() throws IvjException {
        return myType.getupSource();
    }

    /**
     * getMethodBuilders method comment.
     */
    public java.util.Enumeration getMethodBuilders() {
        return null;
    }

    /**
     * getSource method comment.
     */
    public String getSource() {
        return null;
    }

    /**
     * isMarkedForDeletion method comment.
     */
    public boolean isMarkedForDeletion() {
        return false;
    }

    /**
     * markForDeletion method comment.
     */
    public void markForDeletion(boolean arg1) {
    }

    /**
     * removeAllMethodBuilders method comment.
     */
    public void removeAllMethodBuilders() {
    }

    /**
     * removeMethodBuilder method comment.
     */
    public void removeMethodBuilder(MethodBuilder arg1) {
    }

    /**
     * save method comment.
     */
    public void save() throws IvjException {
        mySaveCounter.inc();
    }

    /**
     * saveMerge method comment.
     */
    public void saveMerge() throws IvjException {
    }

    /**
     * setSource method comment.
     */
    public void setSource(String source) {
        mySourceVerifier.setSource(myBuilderName, source);
    }

    public void setupExistingSource(String source) {
        myType.setupSource(source);
    }

    public void verify() {
        mySaveCounter.verify();
    }
}
package test.connextra.util;

import java.util.Enumeration;
import java.util.Vector;

public class TestVector extends Vector {
	public TestVector() {
		super();
	}

	public TestVector(Object[] defaultValues) {
		super();

		for (int i = 0; i < defaultValues.length; i++) {
			addElement(defaultValues[i]);
		}
	}

	public TestVector(Object defaultValue) {
		this(new Object[] {defaultValue});
	}

	public TestVector(Object defaultValue1, Object defaultValue2) {
		this(new Object[] {defaultValue1, defaultValue2});
	}

	public TestVector(Object defaultValue1, Object defaultValue2, Object defaultValue3) {
		this(new Object[] {defaultValue1, defaultValue2, defaultValue3});
	}

	public TestVector(Object defaultValue1, Object defaultValue2, Object defaultValue3, Object defaultValue4) {
		this(new Object[] {defaultValue1, defaultValue2, defaultValue3, defaultValue4});
	}

	public TestVector(Object defaultValue1, Object defaultValue2, Object defaultValue3, Object defaultValue4, Object defaultValue5) {
		this(new Object[] {defaultValue1, defaultValue2, defaultValue3, defaultValue4, defaultValue5});
	}

	public TestVector(Object defaultValue1, Object defaultValue2, Object defaultValue3, Object defaultValue4, Object defaultValue5, Object defaultValue6) {
		this(new Object[] {defaultValue1, defaultValue2, defaultValue3, defaultValue4, defaultValue5, defaultValue6});
	}

	public TestVector(Object defaultValue1, Object defaultValue2, Object defaultValue3, Object defaultValue4, Object defaultValue5, Object defaultValue6, 
					  Object defaultValue7) {
		this(new Object[] {defaultValue1, defaultValue2, defaultValue3, defaultValue4, defaultValue5, defaultValue6, defaultValue7});
	}

	public TestVector(Enumeration defaultValues) {
		while (defaultValues.hasMoreElements()) {
			addElement(defaultValues.nextElement());
		}
	}
}
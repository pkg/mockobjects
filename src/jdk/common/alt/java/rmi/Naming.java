package alt.java.rmi;

import java.rmi.Remote;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.AlreadyBoundException;

public interface Naming {
    Remote lookup(String name) throws NotBoundException,
            java.net.MalformedURLException,
            RemoteException;

    void bind(String name, Remote obj)
            throws AlreadyBoundException,
            java.net.MalformedURLException,
            RemoteException;

    void unbind(String name)
            throws RemoteException,
            NotBoundException,
            java.net.MalformedURLException;

    void rebind(String name, Remote obj)
            throws RemoteException, java.net.MalformedURLException;

    String[] list(String name)
            throws RemoteException, java.net.MalformedURLException;
}

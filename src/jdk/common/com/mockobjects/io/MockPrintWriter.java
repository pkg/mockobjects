package com.mockobjects.io;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import com.mockobjects.ExpectationCounter;
import com.mockobjects.ExpectationSegment;
import com.mockobjects.Verifiable;
import com.mockobjects.util.Verifier;


/**
 * @author steve@m3p.co.uk
 * @author Francois Beausoleil (fbos@users.sourceforge.net)
 */

public class MockPrintWriter extends PrintWriter implements Verifiable {
    private ExpectationSegment mySegment = new ExpectationSegment("String segment");
    private ExpectationCounter myCloseCalls = new ExpectationCounter("close calls");
    private ExpectationCounter myFlushCalls = new ExpectationCounter("flush calls");

    public MockPrintWriter() {
        this(new StringWriter());
    }

    private MockPrintWriter(Writer writer) {
        super(writer);
    }

    public void setExpectedCloseCalls(int calls) {
        myCloseCalls.setExpected(calls);
    }

    public void setExpectedSegment(String aString) {
        mySegment.setExpected(aString);
    }

    public void write(String s) {
        super.write(s);
        mySegment.setActual(s);
    }

    public void setExpectedFlushCalls(int calls) {
        myFlushCalls.setExpected(calls);
    }

    public void flush() {
        super.flush();
        myFlushCalls.inc();
    }

    public void close() {
        super.close();
        myCloseCalls.inc();
    }

    public void verify() {
        Verifier.verifyObject(this);
    }
}

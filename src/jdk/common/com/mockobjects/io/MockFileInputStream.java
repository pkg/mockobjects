package com.mockobjects.io;

import java.io.*;

import junit.framework.*;

import com.mockobjects.*;

public class MockFileInputStream extends FileInputStream {
	public static final String KNOWN_FILE = "c:\\autoexec.bat";

	private String myContents = "";
	private int myIndex = 0;
	
	private MockFileInputStream() throws FileNotFoundException {
		// Unfortunately as there is no interface we have to call the supertype, 
		// which requires a file.
		super(KNOWN_FILE);
	}
	
	public static MockFileInputStream newMockFileInputStream(){
		try {
			return new MockFileInputStream();
		} catch (FileNotFoundException e) {
			throw new AssertionFailedError("couldn't create MockFileInputStream (requires known file: " + KNOWN_FILE +") "+e);
		}
	}
	
	public int available() throws IOException {
		return myContents.length();
	}
	public void mark(int readlimit) {
		MockObject.notYetImplemented("MockFileInputStream.mark");
	}
	public int read() throws IOException {
		if (myIndex == myContents.length())
			return -1;
		return myContents.charAt(myIndex++);
	}
	public int read(byte b[]) throws IOException {
		if (myIndex == myContents.length())
			return -1;

		for (int i = 0; i < myContents.length(); i++) {
			b[i] = (byte) read();
		}
		return myContents.length();
	}
	public int read(byte b[], int off, int len) throws IOException {
		if (myIndex == myContents.length())
			return -1;

		int bytesRead = 0;
		for (int i = off; i < len; i++) {
			b[i] = (byte) myContents.charAt(myIndex);
			bytesRead++;
			myIndex++;
			if (myIndex == myContents.length()) {
				break;
			}
		}
		return bytesRead;
	}
	public void setupContents(String contents) {
		myContents = contents;
		myIndex = 0;
	}
	public long skip(long n) throws IOException {
		MockObject.notYetImplemented("MockFileInputStream.skip");
		return -1;
	}
}

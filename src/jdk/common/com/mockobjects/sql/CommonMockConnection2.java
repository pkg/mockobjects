/*
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2002 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

// ------------------------------------------------------------------------ 78

/**
 * Abstract Connection for use with mock testing.
 * Can verify the expected SQL strings with PreparedStatement or Statement
 * instances (mock versions can be used).
 * Can verify the state of closed and autoCommit.
 * Can verify the number of calls to close, commit, createStatement, and
 * rollBack.
 * Exceptions to throw can be registered for calling createStatement,
 * preparedStatement, or close.
 * Several less-often-used methods are not implemented.
 * If a notImplemented method is are needed for your project, please submit
 * a patch.
 * @see <a href="http://java.sun.com/j2se/1.3/docs/api/java/sql/Connection.html">javax.sql.Connection</a>
 * @author
 * @author Jeff Martin
 * @author Ted Husted
 * @version $Revision: 1.2 $ $Date: 2002/12/09 18:20:13 $
 */
package com.mockobjects.sql;

import com.mockobjects.ReturnObjectBag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

abstract class CommonMockConnection2 extends CommonMockConnection implements Connection {

    private final ReturnObjectBag myPreparedStatements;

    public CommonMockConnection2() {
        this(CommonMockConnection2.class.getName());
    }

    public CommonMockConnection2(String name) {
        super(name);
        myPreparedStatements = new ReturnObjectBag(name+".preparedStatements");
    }

    /**
     * Adds a PreparedStatement to be return by prepareStatement
     */
    public void setupAddPreparedStatement(String sql, PreparedStatement prepared) {
        myPreparedStatements.putObjectToReturn(sql, prepared);
    }

    /**
     * Throws a statement exception if one has been registered
     * (@see throwStatementExceptionIfAny)
     * or returns the next PreparedStatement instance passed to
     * setupAddPreparedStatement.
     */
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        throwStatementExceptionIfAny();
        return (PreparedStatement) myPreparedStatements.getNextReturnObject(sql);
    }
}

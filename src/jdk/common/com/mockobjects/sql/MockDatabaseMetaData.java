package com.mockobjects.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mockobjects.MockObject;

public class MockDatabaseMetaData extends MockObject
implements DatabaseMetaData {

    private String myDriverName;

    public void setupDriverName(String driverName) {
        myDriverName = driverName;
    }

    public boolean supportsANSI92FullSQL() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsANSI92EntryLevelSQL() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsStatementPooling() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsMixedCaseQuotedIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsSubqueriesInComparisons() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsMultipleOpenResults() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getProcedures(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public String getSystemFunctions() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsUnionAll() throws SQLException {
        notImplemented();
        return false;
    }

    public int getDatabaseMinorVersion() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsResultSetConcurrency(int a0, int a1)
    throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getCatalogs() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean usesLocalFilePerTable() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsResultSetHoldability(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getIndexInfo(String a0, String a1, String a2, boolean a3,
    boolean a4) throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsExpressionsInOrderBy() throws SQLException {
        notImplemented();
        return false;
    }

    public String getDatabaseProductName() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsIntegrityEnhancementFacility() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxBinaryLiteralLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsLimitedOuterJoins() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean storesUpperCaseQuotedIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsMultipleResultSets() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsFullOuterJoins() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean ownDeletesAreVisible(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getPrimaryKeys(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public int getMaxColumnsInIndex() throws SQLException {
        notImplemented();
        return 0;
    }

    public ResultSet getExportedKeys(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public boolean locatorsUpdateCopy() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getTables(String a0, String a1, String a2, String[] a3)
    throws SQLException {
        notImplemented();
        return null;
    }

    public int getMaxColumnsInTable() throws SQLException {
        notImplemented();
        return 0;
    }

    public String getDriverVersion() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsBatchUpdates() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxTablesInSelect() throws SQLException {
        notImplemented();
        return 0;
    }

    public Connection getConnection() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsNonNullableColumns() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxColumnsInOrderBy() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsOrderByUnrelated() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsCatalogsInIndexDefinitions() throws SQLException {
        notImplemented();
        return false;
    }

    public int getJDBCMajorVersion() throws SQLException {
        notImplemented();
        return 0;
    }

    public String getExtraNameCharacters() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean nullsAreSortedLow() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getAttributes(String a0, String a1, String a2, String a3)
    throws SQLException {
        notImplemented();
        return null;
    }

    public boolean updatesAreDetected(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsDataManipulationTransactionsOnly()
    throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxConnections() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsOpenCursorsAcrossCommit() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getUDTs(String a0, String a1, String a2, int[] a3)
    throws SQLException {
        notImplemented();
        return null;
    }

    public ResultSet getTableTypes() throws SQLException {
        notImplemented();
        return null;
    }

    public int getResultSetHoldability() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean insertsAreDetected(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getSchemas() throws SQLException {
        notImplemented();
        return null;
    }

    public int getMaxCatalogNameLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public int getDefaultTransactionIsolation() throws SQLException {
        notImplemented();
        return 0;
    }

    public String getURL() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean usesLocalFiles() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsExtendedSQLGrammar() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsSchemasInProcedureCalls() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getSuperTypes(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public int getMaxStatements() throws SQLException {
        notImplemented();
        return 0;
    }

    public int getMaxCharLiteralLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsTransactionIsolationLevel(int a0)
    throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsTableCorrelationNames() throws SQLException {
        notImplemented();
        return false;
    }

    public String getSearchStringEscape() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean deletesAreDetected(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsColumnAliasing() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean isReadOnly() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsLikeEscapeClause() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean storesLowerCaseIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean storesMixedCaseQuotedIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsMultipleTransactions() throws SQLException {
        notImplemented();
        return false;
    }

    public String getProcedureTerm() throws SQLException {
        notImplemented();
        return null;
    }

    public int getMaxStatementLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean nullPlusNonNullIsNull() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsGroupByBeyondSelect() throws SQLException {
        notImplemented();
        return false;
    }

    public int getDriverMajorVersion() {
        notImplemented();
        return 0;
    }

    public boolean supportsCoreSQLGrammar() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsPositionedUpdate() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsCatalogsInPrivilegeDefinitions()
    throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsGetGeneratedKeys() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getImportedKeys(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public String getIdentifierQuoteString() throws SQLException {
        notImplemented();
        return null;
    }

    public ResultSet getSuperTables(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public boolean nullsAreSortedHigh() throws SQLException {
        notImplemented();
        return false;
    }

    public String getNumericFunctions() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsSchemasInIndexDefinitions() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsGroupByUnrelated() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getTypeInfo() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsTransactions() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsPositionedDelete() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxProcedureNameLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsOuterJoins() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsOpenCursorsAcrossRollback() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean storesMixedCaseIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public int getDatabaseMajorVersion() throws SQLException {
        notImplemented();
        return 0;
    }

    public int getMaxColumnsInSelect() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean dataDefinitionIgnoredInTransactions() throws SQLException {
        notImplemented();
        return false;
    }

    public String getCatalogTerm() throws SQLException {
        notImplemented();
        return null;
    }

    public int getMaxRowSize() throws SQLException {
        notImplemented();
        return 0;
    }

    public String getUserName() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean allProceduresAreCallable() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean nullsAreSortedAtStart() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean storesUpperCaseIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsCatalogsInDataManipulation() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getBestRowIdentifier(String a0, String a1, String a2,
    int a3, boolean a4) throws SQLException {
        notImplemented();
        return null;
    }

    public boolean storesLowerCaseQuotedIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean nullsAreSortedAtEnd() throws SQLException {
        notImplemented();
        return false;
    }

    public String getSQLKeywords() throws SQLException {
        notImplemented();
        return null;
    }

    public ResultSet getCrossReference(String a0, String a1, String a2,
    String a3, String a4, String a5) throws SQLException {
        notImplemented();
        return null;
    }

    public boolean isCatalogAtStart() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsNamedParameters() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsSubqueriesInIns() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getVersionColumns(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public boolean othersInsertsAreVisible(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsANSI92IntermediateSQL() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsCatalogsInTableDefinitions() throws SQLException {
        notImplemented();
        return false;
    }

    public int getJDBCMinorVersion() throws SQLException {
        notImplemented();
        return 0;
    }

    public String getStringFunctions() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsDataDefinitionAndDataManipulationTransactions()
    throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getColumnPrivileges(String a0, String a1, String a2,
    String a3) throws SQLException {
        notImplemented();
        return null;
    }

    public boolean dataDefinitionCausesTransactionCommit()
    throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsSubqueriesInExists() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxUserNameLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public int getMaxIndexLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public String getDriverName() throws SQLException {
        return myDriverName;
    }

    public boolean othersUpdatesAreVisible(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsSavepoints() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsMixedCaseIdentifiers() throws SQLException {
        notImplemented();
        return false;
    }

    public String getCatalogSeparator() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsSchemasInPrivilegeDefinitions()
    throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxColumnsInGroupBy() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsDifferentTableCorrelationNames()
    throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsSubqueriesInQuantifieds() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsResultSetType(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public String getDatabaseProductVersion() throws SQLException {
        notImplemented();
        return null;
    }

    public int getSQLStateType() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsSelectForUpdate() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxTableNameLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsUnion() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsOpenStatementsAcrossCommit() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getColumns(String a0, String a1, String a2, String a3)
    throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsAlterTableWithDropColumn() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsStoredProcedures() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean othersDeletesAreVisible(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public String getSchemaTerm() throws SQLException {
        notImplemented();
        return null;
    }

    public int getDriverMinorVersion() {
        notImplemented();
        return 0;
    }

    public int getMaxSchemaNameLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean doesMaxRowSizeIncludeBlobs() throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getTablePrivileges(String a0, String a1, String a2)
    throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsSchemasInDataManipulation() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxCursorNameLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean supportsAlterTableWithAddColumn() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsOpenStatementsAcrossRollback() throws SQLException {
        notImplemented();
        return false;
    }

    public int getMaxColumnNameLength() throws SQLException {
        notImplemented();
        return 0;
    }

    public boolean ownInsertsAreVisible(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsGroupBy() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsConvert(int a0, int a1) throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsConvert() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsCorrelatedSubqueries() throws SQLException {
        notImplemented();
        return false;
    }

    public String getTimeDateFunctions() throws SQLException {
        notImplemented();
        return null;
    }

    public boolean supportsSchemasInTableDefinitions() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsCatalogsInProcedureCalls() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean allTablesAreSelectable() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean supportsMinimumSQLGrammar() throws SQLException {
        notImplemented();
        return false;
    }

    public boolean ownUpdatesAreVisible(int a0) throws SQLException {
        notImplemented();
        return false;
    }

    public ResultSet getProcedureColumns(String a0, String a1, String a2,
    String a3) throws SQLException {
        notImplemented();
        return null;
    }

}

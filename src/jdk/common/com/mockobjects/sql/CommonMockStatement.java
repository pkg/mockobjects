package com.mockobjects.sql;

import com.mockobjects.ExpectationCounter;
import com.mockobjects.MockObject;
import com.mockobjects.ReturnObjectBag;

import java.sql.*;

/**
 * @version $Revision: 1.4 $
 */
abstract class CommonMockStatement extends MockObject implements Statement {
    protected final ExpectationCounter myCloseCalls = new ExpectationCounter("CommonMockStatement.closeCalls");
    private final ReturnObjectBag executeQueryResults = new ReturnObjectBag("executeQuery");
    private final ReturnObjectBag executeUpdateResults = new ReturnObjectBag("executeUpdate");
    private final ReturnObjectBag executeResults = new ReturnObjectBag("execute");

    private int myUpdateCount = 0;
    private SQLException myExecuteException = null;

    private Connection myConnection = null;

    public void addExpectedExecuteQuery(String queryString, ResultSet resultSet) {
        executeQueryResults.putObjectToReturn(queryString, resultSet);
    }

    public void addExpectedExecuteUpdate(String queryString, int updateCount) {
        executeUpdateResults.putObjectToReturn(queryString, updateCount);
    }

    public void addExpectedExecute(String queryString, boolean success) {
        executeResults.putObjectToReturn(queryString, success);
    }

    public void setExpectedCloseCalls(int callCount) {
        myCloseCalls.setExpected(callCount);
    }

    public void setupConnection(Connection conn) {
        myConnection = conn;
    }

    public void setupThrowExceptionOnExecute(SQLException exception) {
        myExecuteException = exception;
    }

    public void setupUpdateCount(int updateCount) {
        myUpdateCount = updateCount;
    }

    protected void innerExecute() throws SQLException {
        if (null != myExecuteException) {
            throw myExecuteException;
        }
    }

    public void close() throws SQLException {
        myCloseCalls.inc();
    }

    public boolean execute(String sql) throws SQLException {
        innerExecute();
        return executeResults.getNextReturnBoolean(sql);
    }

    public ResultSet executeQuery(String sql) throws SQLException {
        innerExecute();
        return (ResultSet) executeQueryResults.getNextReturnObject(sql);
    }

    public int executeUpdate(String sql) throws SQLException {
        innerExecute();
        return executeUpdateResults.getNextReturnInt(sql);
    }

    public int getMaxFieldSize() throws SQLException {
        notImplemented();
        return 0;
    }

    public void setMaxFieldSize(int max) throws SQLException {
        notImplemented();
    }

    public int getMaxRows() throws SQLException {
        notImplemented();
        return 0;
    }

    public void setMaxRows(int max) throws SQLException {
        notImplemented();
    }

    public void setEscapeProcessing(boolean enable) throws SQLException {
        notImplemented();
    }

    public int getQueryTimeout() throws SQLException {
        notImplemented();
        return 0;
    }

    public void setQueryTimeout(int seconds) throws SQLException {
        notImplemented();
    }

    public void cancel() throws SQLException {
        notImplemented();
    }

    public SQLWarning getWarnings() throws SQLException {
        notImplemented();
        return null;
    }

    public void clearWarnings() throws SQLException {
        notImplemented();
    }

    public void setCursorName(String name) throws SQLException {
        notImplemented();
    }

    public ResultSet getResultSet() throws SQLException {
        notImplemented();
        return null;
    }

    public int getUpdateCount() throws SQLException {
        return myUpdateCount;
    }

    public boolean getMoreResults() throws SQLException {
        notImplemented();
        return false;
    }

    public void setFetchDirection(int direction) throws SQLException {
        notImplemented();
    }

    public int getFetchDirection() throws SQLException {
        notImplemented();
        return 0;
    }

    public void setFetchSize(int rows) throws SQLException {
        notImplemented();
    }

    public int getFetchSize() throws SQLException {
        notImplemented();
        return 0;
    }

    public int getResultSetConcurrency() throws SQLException {
        notImplemented();
        return 0;
    }

    public int getResultSetType() throws SQLException {
        notImplemented();
        return 0;
    }

    public void addBatch(String sql) throws SQLException {
        notImplemented();
    }

    public void clearBatch() throws SQLException {
        notImplemented();
    }

    public int[] executeBatch() throws SQLException {
        notImplemented();
        return null;
    }

    public Connection getConnection() throws SQLException {
        return myConnection;
    }
}

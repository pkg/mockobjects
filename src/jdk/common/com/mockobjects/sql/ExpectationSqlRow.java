package com.mockobjects.sql;

import java.sql.SQLException;
import java.util.Map;
import java.util.Iterator;
import com.mockobjects.*;
import com.mockobjects.util.*;

public class ExpectationSqlRow implements Verifiable {
    private ExpectationMap values;

    public ExpectationSqlRow(String name) {
        super();
        values = new ExpectationMap(name);
        values.setExpectNothing();
    }

    public ExpectationSqlRow(String name, Object[] indexedValues) {
        this(name);
        addExpectedIndexedValues(indexedValues);
    }

    public ExpectationSqlRow(String name, String[] columnNames, Object[] values) {
        this(name);
        addExpectedNamedValues(columnNames, values);
    }

    public void addExpectedIndexedValues(Object[] indexedValues) {
        for (int i = 0; i < indexedValues.length; ++i) {
            values.addExpected(new Integer(i + 1), indexedValues[i]);
        }
    }

    public void addExpectedNamedValues(Map map) {
        Iterator columnNames = map.keySet().iterator();
        while (columnNames.hasNext()) {
            Object key = columnNames.next();
            values.addExpected(key, map.get(key));
        }
    }

    public void addExpectedNamedValues(String[] columnNames, Object[] someValues) {
        for (int i = 0; i < someValues.length; ++i) {
            values.addExpected(columnNames[i], someValues[i]);
        }
    }

    public Object get(int oneBasedIndex) throws SQLException {
        return get(new Integer(oneBasedIndex));
    }

    public Object get(Object key) throws SQLException {
        Object result = values.get(key);
        if (result instanceof SQLException) {
            throw (SQLException)result;
        }
        return result;
    }

    public void verify() {
        values.verify();
    }
}

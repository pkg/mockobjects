/*
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2002 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

// ------------------------------------------------------------------------ 78

package com.mockobjects.sql;

import java.sql.*;
import java.util.Calendar;
import java.io.Reader;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;

import com.mockobjects.*;

abstract class CommonMockCallableStatement
    extends CommonMockPreparedStatement implements CallableStatement {

    public void registerOutParameter(int parameterIndex, int sqlType) {
        notImplemented();
    }

    public void registerOutParameter(int parameterIndex, int sqlType, int scale) {
        notImplemented();
    }

    public void registerOutParameter(int parameterIndex, int sqlType, String typeName) {
        notImplemented();
    }

    public void registerOutParameter(String parameterName, int sqlType) {
        notImplemented();
    }

    public void registerOutParameter(String parameterName, int sqlType, int scale) {
        notImplemented();
    }

    public void registerOutParameter(String parameterName, int sqlType, String typeName) {
        notImplemented();
    }

    public boolean wasNull() {
        notImplemented();
        return false;
    }

    public String getString(int parameterIndex) {
        notImplemented();
        return null;
    }

    public String getString(String parameterName) {
        notImplemented();
        return null;
    }

    public boolean getBoolean(int parameterIndex) {
        notImplemented();
        return false;
    }

    public boolean getBoolean(String parameterName) {
        notImplemented();
        return false;
    }

    public byte getByte(int parameterIndex) {
        notImplemented();
        return 0;
    }

    public byte getByte(String parameterName) {
        notImplemented();
        return 0;
    }

    public short getShort(int parameterIndex) {
        notImplemented();
        return 0;
    }

    public short getShort(String parameterName) {
        notImplemented();
        return 0;
    }

    public int getInt(int parameterIndex) {
        notImplemented();
        return 0;
    }

    public int getInt(String parameterName) {
        notImplemented();
        return 0;
    }

    public long getLong(int parameterIndex) {
        notImplemented();
        return 0;
    }

    public long getLong(String parameterName) {
        notImplemented();
        return 0;
    }

    public float getFloat(int parameterIndex) {
        notImplemented();
        return 0;
    }

    public float getFloat(String parameterName) {
        notImplemented();
        return 0;
    }

    public double getDouble(int parameterIndex) {
        notImplemented();
        return 0;
    }

    public double getDouble(String parameterName) {
        notImplemented();
        return 0;
    }

    public BigDecimal getBigDecimal(int parameterIndex) {
        notImplemented();
        return null;
    }

    public BigDecimal getBigDecimal(String parameterName) {
        notImplemented();
        return null;
    }

    public BigDecimal getBigDecimal(int parameterIndex, int scale) {
        notImplemented();
        return null;
    }

    public byte[] getBytes(int parameterIndex) {
        notImplemented();
        return null;
    }

    public byte[] getBytes(String parameterName) {
        notImplemented();
        return null;
    }

    public Date getDate(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Date getDate(String parameterName) {
        notImplemented();
        return null;
    }

    public Date getDate(int parameterIndex, Calendar cal) {
        notImplemented();
        return null;
    }

    public Date getDate(String parameterName, Calendar cal) {
        notImplemented();
        return null;
    }

    public Time getTime(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Time getTime(String parameterName) {
        notImplemented();
        return null;
    }

    public Time getTime(int parameterIndex, Calendar cal) {
        notImplemented();
        return null;
    }

    public Time getTime(String parameterName, Calendar cal) {
        notImplemented();
        return null;
    }

    public Timestamp getTimestamp(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Timestamp getTimestamp(String parameterName) {
        notImplemented();
        return null;
    }

    public Timestamp getTimestamp(int parameterIndex, Calendar cal) {
        notImplemented();
        return null;
    }

    public Timestamp getTimestamp(String parameterName, Calendar cal) {
        notImplemented();
        return null;
    }

    public Object getObject(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Object getObject(String parameterName) {
        notImplemented();
        return null;
    }

    public Object getObject(int parameterIndex, java.util.Map map) {
        notImplemented();
        return null;
    }

    public Object getObject(String parameterName, java.util.Map map) {
        notImplemented();
        return null;
    }

    public Ref getRef(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Ref getRef(String parameterName) {
        notImplemented();
        return null;
    }

    public Blob getBlob(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Blob getBlob(String parameterName) {
        notImplemented();
        return null;
    }

    public Clob getClob(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Clob getClob(String parameterName) {
        notImplemented();
        return null;
    }

    public Array getArray(int parameterIndex) {
        notImplemented();
        return null;
    }

    public Array getArray(String parameterName) {
        notImplemented();
        return null;
    }

    public URL getURL(int parameterIndex) {
        notImplemented();
        return null;
    }

    public URL getURL(String parameterName) {
        notImplemented();
        return null;
    }

    public void setAsciiStream(String parameterName, InputStream x, int length) {
        notImplemented();
    }

    public void setBigDecimal(String parameterName, BigDecimal x) {
        notImplemented();
    }

    public void setBinaryStream(String parameterName, InputStream x, int length) {
        notImplemented();
    }

    public void setBoolean(String parameterName, boolean x) {
        notImplemented();
    }

    public void setByte(String parameterName, byte x) {
        notImplemented();
    }

    public void setBytes(String parameterName, byte[] x) {
        notImplemented();
    }

    public void setCharacterStream(String parameterName, Reader reader, int length) {
        notImplemented();
    }

    public void setDate(String parameterName, Date x) {
        notImplemented();
    }

    public void setDate(String parameterName, Date x, Calendar cal) {
        notImplemented();
    }

    public void setDouble(String parameterName, double x) {
        notImplemented();
    }

    public void setFloat(String parameterName, float x) {
        notImplemented();
    }

    public void setInt(String parameterName, int x) {
        notImplemented();
    }

    public void setLong(String parameterName, long x) {
        notImplemented();
    }

    public void setNull(String parameterName, int sqlType) {
        notImplemented();
    }

    public void setNull(String parameterName, int sqlType, String typeName) {
        notImplemented();
    }

    public void setObject(String parameterName, Object x) {
        notImplemented();
    }

    public void setObject(String parameterName, Object x, int targetSqlType) {
        notImplemented();
    }

    public void setObject(String parameterName, Object x, int targetSqlType, int scale) {
        notImplemented();
    }

    public void setShort(String parameterName, short x) {
        notImplemented();
    }

    public void setString(String parameterName, String x) {
        notImplemented();
    }

    public void setTime(String parameterName, Time x) {
        notImplemented();
    }

    public void setTime(String parameterName, Time x, Calendar cal) {
        notImplemented();
    }

    public void setTimestamp(String parameterName, Timestamp x) {
        notImplemented();
    }

    public void setTimestamp(String parameterName, Timestamp x, Calendar cal) {
        notImplemented();
    }

    public void setURL(String parameterName, URL val) {
        notImplemented();
    }
}
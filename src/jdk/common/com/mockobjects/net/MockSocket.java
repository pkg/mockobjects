package com.mockobjects.net;

import alt.java.net.Socket;
import com.mockobjects.ExpectationCounter;
import com.mockobjects.ExpectationValue;
import com.mockobjects.MockObject;
import com.mockobjects.ReturnValue;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketException;

public class MockSocket extends MockObject implements Socket {
    private final ExpectationValue mySoTimeout = new ExpectationValue("so timeout");
    private final ReturnValue myInputStream = new ReturnValue("input stream");
    private final ReturnValue myOutputStream = new ReturnValue("output stream");
    private final ExpectationCounter myCloseCalls = new ExpectationCounter("close calls");

    public InetAddress getInetAddress() {
        notImplemented();
        return null;
    }

    public InetAddress getLocalAddress() {
        notImplemented();
        return null;
    }

    public int getPort() {
        notImplemented();
        return 0;
    }

    public int getLocalPort() {
        notImplemented();
        return 0;
    }

    public void setupGetInputStream(InputStream anInputStream) {
        myInputStream.setValue(anInputStream);
    }

    public InputStream getInputStream() throws IOException {
        return (InputStream) myInputStream.getValue();
    }

    public void setupGetOutputStream(OutputStream anOutputStream) {
        myOutputStream.setValue(anOutputStream);
    }

    public OutputStream getOutputStream() throws IOException {
        return (OutputStream) myOutputStream.getValue();
    }

    public void setTcpNoDelay(boolean on) throws SocketException {
        notImplemented();
    }

    public boolean getTcpNoDelay() throws SocketException {
        notImplemented();
        return false;
    }

    public void setSoLinger(boolean on, int linger) throws SocketException {
        notImplemented();
    }

    public int getSoLinger() throws SocketException {
        notImplemented();
        return 0;
    }

    public void setExpectedSoTimeout(int aSoTimeout) {
        mySoTimeout.setExpected(aSoTimeout);
    }

    public void setSoTimeout(int aSoTimeout) throws SocketException {
        mySoTimeout.setActual(aSoTimeout);
    }

    public int getSoTimeout() throws SocketException {
        notImplemented();
        return 0;
    }

    public void setSendBufferSize(int size)
        throws SocketException {
        notImplemented();
    }

    public int getSendBufferSize() throws SocketException {
        notImplemented();
        return 0;
    }

    public void setReceiveBufferSize(int size)
        throws SocketException {
        notImplemented();
    }

    public int getReceiveBufferSize()
        throws SocketException {
        notImplemented();
        return 0;
    }

    public void setKeepAlive(boolean on) throws SocketException {
        notImplemented();
    }

    public boolean getKeepAlive() throws SocketException {
        notImplemented();
        return false;
    }

    public void setExpectedCloseCalls(int aCount) {
        myCloseCalls.setExpected(aCount);
    }

    public void close() throws IOException {
        myCloseCalls.inc();
    }

    public void shutdownInput() throws IOException {
        notImplemented();
    }

    public void shutdownOutput() throws IOException {
        notImplemented();
    }
}

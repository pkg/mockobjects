package com.mockobjects.net;

import alt.java.net.SocketFactory;
import alt.java.net.Socket;
import com.mockobjects.MockObject;
import com.mockobjects.ExpectationValue;
import com.mockobjects.ReturnValue;

public class MockSocketFactory extends MockObject implements SocketFactory {
    private final ExpectationValue myHost = new ExpectationValue("host");
    private final ExpectationValue myPort = new ExpectationValue("port");
    private final ReturnValue mySocket = new ReturnValue("socket");

    public void setupCreateSocket(Socket aSocket) {
        mySocket.setValue(aSocket);
    }

    public void setExpectedHost(String aHost) {
        myHost.setExpected(aHost);
    }

    public void setExpectedPort(String aPort) {
        myPort.setExpected(aPort);
    }

    public Socket createSocket(String aHost, int aPort) {
        myHost.setActual(aHost);
        myPort.setActual(aPort);
        return (Socket) mySocket.getValue();
    }
}

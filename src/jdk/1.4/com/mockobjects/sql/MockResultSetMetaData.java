package com.mockobjects.sql;

/**
 * Minimal implementation of ResultSetMetaData for testing.
 * Supports column count, column names, and column sql types.
 * @see <a href=" http://java.sun.com/j2se/1.3/docs/api/java/sql/ResultSetMetaData.html">javax.sql.ResultSetMetaData</a>.
 * @author Jeff Martin
 * @author Ted Husted
 * @version $Revision: 1.3 $
 */
public class MockResultSetMetaData extends CommonMockResultSetMetaData {
}

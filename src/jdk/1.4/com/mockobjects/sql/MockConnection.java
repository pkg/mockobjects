package com.mockobjects.sql;

import java.sql.SQLException;
import java.sql.Savepoint;

/**
 * @deprecated Use temporary class MockConnection2
 * @see MockConnection2
 */
public class MockConnection extends CommonMockConnection{

    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        notImplemented();
    }

    public void rollback(Savepoint savepoint) throws SQLException {
        notImplemented();
    }

    public Savepoint setSavepoint() throws SQLException {
        notImplemented();
        return null;
    }

    public Savepoint setSavepoint(String name) throws SQLException {
        notImplemented();
        return null;
    }
}

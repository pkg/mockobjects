package com.mockobjects.sql;

import java.sql.SQLException;
import java.sql.Savepoint;

/**
 * MockConnection2 is a tempary replacement for the MockConnection.
 * It differs from the MockConnection in the way in which PreparedStatements
 * are handled. The changes are significant enough to break compatiblity with
 * existing testcases so MockConnection2 provides a migration path to the new
 * system of handling PreparedStatements.
 * This class will eventually be merged back into MockConnection when it is
 * felt systems using the classes have been provided with enough time to migrate.
 */
public class MockConnection2 extends CommonMockConnection2{
    private final MockConnection connection = new MockConnection();

    public MockConnection2() {
    }

    public MockConnection2(String name) {
        super(name);
    }

    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        connection.releaseSavepoint(savepoint);
    }

    public void rollback(Savepoint savepoint) throws SQLException {
        connection.rollback(savepoint);
    }

    public Savepoint setSavepoint() throws SQLException {
        return connection.setSavepoint();
    }

    public Savepoint setSavepoint(String name) throws SQLException {
        return connection.setSavepoint();
    }
}

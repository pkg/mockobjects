/*  Copyright (c) 2002 Nat Pryce. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:33 PM
 */
package com.mockobjects.constraint;


/** Is the value null?
 */
public class IsNull implements Constraint
{
    public IsNull() {
    }
    
    public boolean eval(Object o) {
        return o == null;
    }
    
    public String toString() {
        return "null";
    }
}


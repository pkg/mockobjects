/*  Copyright (c) 2002 Nat Pryce. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:21 PM
 */
package com.mockobjects.constraint;

/** A constraint over acceptable values.
 */
public interface Constraint 
{
    /** Evaluates the constraint for argument <var>o</var>.
     * 
     *  @return
     *      <code>true</code> if <var>o</var> meets the constraint,
     *      <code>false</code> if it does not.
     */
    boolean eval( Object o );
}

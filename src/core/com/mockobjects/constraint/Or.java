/*  Copyright (c) 2002 B13media Ltd. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:49 PM
 */
package com.mockobjects.constraint;

/** Calculates the logical disjunction of two constraints.
 *  Evaluation is shortcut, so that the second constraint is not
 *  called if the first constraint returns <code>true</code>.
 */
public class Or
    implements Constraint
{
    Constraint _p1, _p2;
    
    public Or( Constraint p1, Constraint p2 ) {
        _p1 = p1;
        _p2 = p2;
    }
    
    public boolean eval( Object o ) {
        return _p1.eval(o) || _p2.eval(o);
    }
    
    public String toString() {
        return "(" + _p1 + " or " + _p2 + ")";
    }
}

/*
 * Created on 07-Apr-2003
 */
package com.mockobjects.dynamic;

/**
 * @author dev
 */
public class ThrowStub 
	extends CallStub
{
	private Throwable throwable;
	
	public ThrowStub( Throwable throwable ) {
		this.throwable = throwable;
	}
	
	public Object call(Mock mock, String methodName, Object[] args) throws Throwable {
		throw throwable;
	}
	
	public String getDescription() {
		return "throws <" + throwable + ">"; 
	}
}

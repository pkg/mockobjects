/*
 * Created on 11-Apr-2003
 */
package com.mockobjects.dynamic;


public class VoidStub extends CallStub {

	public String getDescription() {
		return "returns <void>";
	}
	
	public Object call(Mock mock, String methodName, Object[] args) throws Throwable {
		return null;
	}

}

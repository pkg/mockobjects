package com.mockobjects.dynamic;

public interface CallableAddable extends Callable {
	void addExpect(Callable call);
	void addMatch(Callable call);

    /**
     * Resets all expected calls and expected matches.
     */
    void reset();
}
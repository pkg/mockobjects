/*
 * Created on 07-Apr-2003
 */
package com.mockobjects.dynamic;

/**
 * @author dev
 */
public class ReturnStub 
	extends CallStub 
{
	private Object result;
	
	public ReturnStub( Object result ) {
		this.result = result;
	}

	public Object call(Mock mock, String methodName, Object[] args) throws Throwable {
		return result;
	}
	
	public String getDescription() {
		return "returns <" + result + ">";
	}
}

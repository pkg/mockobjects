package com.mockobjects.dynamic;

public interface CallFactory {
	Callable createReturnStub( Object result );
	Callable createThrowStub( Throwable throwable );
	Callable createVoidStub();
	Callable createCallExpectation( Callable call );
	Callable createCallSignature( String methodName, ConstraintMatcher constraints, Callable call );
}

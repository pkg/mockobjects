package test.mockobjects;

import com.mockobjects.util.SuiteBuilder;
import com.mockobjects.util.TestCaseMo;
import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * JUnit test case for AllTests
 */

public class AllTests extends TestCaseMo {
    private static final Class THIS = AllTests.class;

    public AllTests(String name) {
        super(name);
    }

    public static void addTestAssertMo(TestSuite suite) {
        suite.addTest(TestAssertMo.suite());
    }

    public static void addTestExpectationCounter(TestSuite suite) {
        suite.addTest(TestExpectationCounter.suite());
    }

    public static void addTestExpectationList(TestSuite suite) {
        suite.addTest(TestExpectationList.suite());
    }

    public static void addTestExpectationMap(TestSuite suite) {
        suite.addTestSuite(TestExpectationMap.class);
    }

    public static void addTestExpectationSegment(TestSuite suite) {
        suite.addTest(TestExpectationSegment.suite());
    }

    public static void addTestExpectationSet(TestSuite suite) {
        suite.addTest(TestExpectationSet.suite());
    }

    public static void addTestExpectationValue(TestSuite suite) {
        suite.addTest(TestExpectationValue.suite());
    }

    public static void addTestExpectationDoubleValue(TestSuite suite) {
        suite.addTest(TestExpectationDoubleValue.suite());
    }

    public static void addTestMapEntry(TestSuite suite) {
        suite.addTest(TestMapEntry.suite());
    }

    public static void addTestNull(TestSuite suite) {
        suite.addTestSuite(TestNull.class);
    }

    public static void addTestVerifier(TestSuite suite) {
        suite.addTest(TestVerifier.suite());
    }

    public static void addTestReturnObjectList(TestSuite suite) {
        suite.addTestSuite(TestReturnObjectList.class);
    }
    
    public static void main(String[] args) {
        start(new String[] { THIS.getName()});
    }

    public static Test suite() {
        return SuiteBuilder.buildTest(THIS);
    }
}

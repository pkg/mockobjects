package test.mockobjects.dynamic;

import com.mockobjects.*;
import com.mockobjects.dynamic.ConstraintMatcher;

public class MockConstraintMatcher implements ConstraintMatcher{
	private ExpectationCounter myMatchesCalls = new ExpectationCounter("MockConstraintMatcher.matches(Object[])");
	private ReturnValues myActualMatchesReturnValues = new ReturnValues("MockConstraintMatcher.matches(Object[])", true);
	private ExpectationList myMatchesParameter0Values = new ExpectationList("MockConstraintMatcher.matches(Object[]) java.lang.Object");
	private ExpectationCounter myGetConstraintsCalls = new ExpectationCounter("MockConstraintMatcher.getConstraints()");
	private ReturnValues myActualGetConstraintsReturnValues = new ReturnValues("MockConstraintMatcher.getConstraints()", true);

	public void setExpectedMatchesCalls(int calls){
		myMatchesCalls.setExpected(calls);
	}

	public void addExpectedMatches(Object[] arg0){
		myMatchesParameter0Values.addExpected(arg0);
	}

	public boolean matches(Object[] arg0){
		myMatchesCalls.inc();
		myMatchesParameter0Values.addActual(arg0);
		Object nextReturnValue = myActualMatchesReturnValues.getNext();
		if (nextReturnValue instanceof ExceptionalReturnValue && ((ExceptionalReturnValue)nextReturnValue).getException() instanceof RuntimeException)
			throw (RuntimeException)((ExceptionalReturnValue)nextReturnValue).getException();
		return ((Boolean) nextReturnValue).booleanValue();
	}

	public void setupExceptionMatches(Throwable arg){
		myActualMatchesReturnValues.add(new ExceptionalReturnValue(arg));
	}

	public void setupMatches(boolean arg){
		myActualMatchesReturnValues.add(new Boolean(arg));
	}

	public void setExpectedGetConstraintsCalls(int calls){
		myGetConstraintsCalls.setExpected(calls);
	}

	public Object[] getConstraints(){
		myGetConstraintsCalls.inc();
		Object nextReturnValue = myActualGetConstraintsReturnValues.getNext();
		if (nextReturnValue instanceof ExceptionalReturnValue && ((ExceptionalReturnValue)nextReturnValue).getException() instanceof RuntimeException)
			throw (RuntimeException)((ExceptionalReturnValue)nextReturnValue).getException();
		return (Object[]) nextReturnValue;
	}

	public void setupExceptionGetConstraints(Throwable arg){
		myActualGetConstraintsReturnValues.add(new ExceptionalReturnValue(arg));
	}

	public void setupGetConstraints(Object[] arg){
		myActualGetConstraintsReturnValues.add(arg);
	}

	public void verify(){
		myMatchesCalls.verify();
		myMatchesParameter0Values.verify();
		myGetConstraintsCalls.verify();
	}
}

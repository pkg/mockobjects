/*
 * Created on 04-Apr-2003
 */
package test.mockobjects.dynamic;

/**
 * @author dev
 */
public interface DummyInterface {
	public String twoArgMethod( String arg1, String arg2 ) throws Throwable;
	public String oneArgMethod( String arg1);
	
	public void noArgMethodVoid();
	public String noArgMethod();
}

package test.mockobjects;

import com.mockobjects.util.TestCaseMo;
import com.mockobjects.ReturnValue;
import junit.framework.AssertionFailedError;


public class TestReturnValue extends TestCaseMo {
    private final ReturnValue value = new ReturnValue(getName());

    public void testGetNull(){
        value.setValue(null);
        assertTrue(value.getValue()==null);
    }

    public void testGetValue(){
        value.setValue(this);
        assertEquals(this, value.getValue());
    }

    public void testGetBooleanValue(){
        value.setValue(true);
        assertTrue(value.getBooleanValue());
    }

    public void testIntValue(){
        value.setValue(13);
        assertEquals(13, value.getIntValue());
    }

    public void testLongValue(){
        long now  = System.currentTimeMillis();
        value.setValue(now);
        assertEquals(now, value.getLongValue());
        value.getIntValue();
    }

    public void testValueNotSet() {
        try {
            value.getValue();
            fail("Error not thrown");
        } catch (AssertionFailedError e) {
            assertEquals("The return value \"" + getName() + "\" has not been set.", e.getMessage());
        }
    }

    public TestReturnValue(String name) {
        super(name);
    }

    public static void main(String[] args) {
        start(new String[] { TestReturnValue.class.getName()});
    }

}

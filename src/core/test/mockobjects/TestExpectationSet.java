package test.mockobjects;

import com.mockobjects.ExpectationSet;
import com.mockobjects.MapEntry;
import junit.framework.Test;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.Vector;

public class TestExpectationSet extends TestExpectationCollection {
    private static final Class THIS = TestExpectationSet.class;

    public TestExpectationSet(String name) {
        super(name);
        myExpectation = new ExpectationSet(name);
    }

    public void lookAtTheSuperclassForTests() {
    }

    public static void main(String[] args) {
        start(new String[] { THIS.getName()});
    }

    public static Test suite() {
        return new TestSuite(THIS);
    }

    public void testMultiUnsorted() {
        myExpectation.addExpectedMany(new String[] { "A", "B" });

        myExpectation.addActualMany(new String[] { "A", "B" });

        myExpectation.verify();
    }

    public void testChangingHashcode() {
        final Vector value = new Vector();

        myExpectation.addExpected(new MapEntry("key", value));
        myExpectation.addActual(new MapEntry("key", value));

        value.add(getName());

        myExpectation.verify();
    }

    public void testChanginHashcodeImediateCheck() {
        final Vector value = new Vector();

        myExpectation.addExpected(new MapEntry("key", value));
        value.add(getName());
        myExpectation.addActual(new MapEntry("key", value));

        myExpectation.verify();
    }

    public void testMultiUnsortedSet() {
        myExpectation.addExpectedMany(new String[] { "A", "B" });

        myExpectation.addActualMany(new String[] { "A", "B", "A", "B" });

        myExpectation.verify();
    }

    public void testUnsorted() {
        myExpectation.addExpected("A");
        myExpectation.addExpected("B");

        myExpectation.addActual("B");
        myExpectation.addActual("A");

        myExpectation.verify();
    }

    public void testUnsortedSet() {
        myExpectation.addExpected("A");
        myExpectation.addExpected("B");

        myExpectation.addActual("A");
        myExpectation.addActual("B");
        myExpectation.addActual("A");
        myExpectation.addActual("B");

        myExpectation.verify();
    }
}

package com.mockobjects.servlet;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public class MockPageContext extends PageContext{
    private JspWriter jspWriter;
    private ServletRequest request;
    private HttpSession httpSession;
    private ServletContext servletContext;

    public void release(){
    }

    public JspWriter getOut(){
        return jspWriter;
    }

    public void setJspWriter(JspWriter jspWriter){
        this.jspWriter = jspWriter;
    }

    public void handlePageException(Exception e){
    }

    public ServletContext getServletContext(){
        return servletContext;
    }

    public void setServletContext(ServletContext servletContext){
        this.servletContext = servletContext;
    }

    public int getAttributesScope(String s){
        return -1;
    }

    public void include(String s){
    }

    public void removeAttribute(String s, int i){
    }

    public Enumeration getAttributeNamesInScope(int i){
        return null;
    }

    public void forward(String s){
    }

    public Object getPage(){
        return null;
    }

    public void handlePageException(Throwable t){
    }

    public void setRequest(ServletRequest servletRequest){
        this.request = servletRequest;
    }

    public ServletRequest getRequest(){
        return request;
    }

    public ServletResponse getResponse(){
        return null;
    }

    public void removeAttribute(String s){
    }

    public Object getAttribute(String s, int i){
        return null;
    }

    public ServletConfig getServletConfig(){
        return null;
    }

    public void initialize(Servlet servlet, ServletRequest servletRequest, ServletResponse servletResponse, String s, boolean b, int i, boolean b2){
    }

    public Object findAttribute(String s) {
        return null;
    }

    public HttpSession getSession() {
        return httpSession;
    }

    public void setSession(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    public void setAttribute(String s, Object o){
    }

    public void setAttribute(String s, Object o, int i) {
    }

    public Object getAttribute(String s) {
        return null;
    }

    public Exception getException() {
            return null;
    }

    public void verify(){
    }
}

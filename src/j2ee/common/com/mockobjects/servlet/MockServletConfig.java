package com.mockobjects.servlet;

import javax.servlet.*;
import junit.framework.*;
import junit.textui.*;
import java.util.*;
import java.io.*;
import java.net.*;
import com.mockobjects.*;

/**
 * @version $Revision: 1.1 $
 */
public class MockServletConfig extends MockObject implements ServletConfig {
    private Dictionary myParameters = new Hashtable();
    private ServletContext servletContext;

    public String getInitParameter(String paramName) {
        return (String)myParameters.get(paramName);
    }

    public void setInitParameter(String paramName, String paramValue) {
        myParameters.put(paramName, paramValue);
    }

    public java.util.Enumeration getInitParameterNames() {
        return myParameters.keys();
    }

    public void setupAddInitParameter(String paramName, String value) {
        myParameters.put(paramName, value);
    }

    public void setupNoParameters() {
        myParameters = new Hashtable();
    }

    public void setServletContext(ServletContext servletContext){
        this.servletContext = servletContext;
    }

    public ServletContext getServletContext(){
        return servletContext;
    }
    public String getServletName(){
        return null;
    }
}


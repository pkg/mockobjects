package com.mockobjects.naming.directory;

import com.mockobjects.*;
import javax.naming.*;
import javax.naming.directory.*;
import java.util.*;

public class MockNamingEnumeration extends MockObject
implements NamingEnumeration{
    private List mySearchResults = new ArrayList();

    private boolean hasMore = true;

    public void setupAddSearchResult(Object object){
        mySearchResults.add(object);
    }

    public Object next() throws NamingException{
        return mySearchResults.remove(0);
    }

    public boolean hasMore() throws NamingException{
        return mySearchResults.size()>0;
    }

    public void close() throws NamingException{
    }

    public boolean hasMoreElements(){
        return mySearchResults.size()>0;
    }

    public Object nextElement(){
        return mySearchResults.remove(0);
    }
}

package com.mockobjects.jms;

import com.mockobjects.*;
import javax.jms.*;

public abstract class MockMessageProducer extends MockObject implements MessageProducer {

  protected final ExpectationCounter myCloseCalls = new ExpectationCounter("MockMessageConsumer.close");

  private JMSException myException;

  public void close() throws JMSException {
    myCloseCalls.inc();
  }

  public int getDeliveryMode() throws JMSException {
    notImplemented();
    return 0;
  }

  public boolean getDisableMessageID() throws JMSException {
    notImplemented();
    return false;
  }

  public boolean getDisableMessageTimestamp() throws JMSException {
    notImplemented();
    return false;
  }

  public int getPriority() throws JMSException {
    notImplemented();
    return 0;
  }

  public long getTimeToLive() throws JMSException {
    notImplemented();
    return 0l;
  }

  public void setDeliveryMode(int deliveryMode) throws JMSException {
    notImplemented();
  }

  public void setDisableMessageID(boolean value) throws JMSException {
    notImplemented();
  }

  public void setDisableMessageTimestamp(boolean value) throws JMSException {
    notImplemented();
  }

  public void setPriority(int defaultPriority) throws JMSException {
    notImplemented();
  }

  public void setTimeToLive(long timeToLive) throws JMSException {
    notImplemented();
  }

  public void setExpectedCloseCalls(int callCount) {
    myCloseCalls.setExpected(callCount);
  }

  public void setupThrowException(JMSException e) {
    myException = e;
  }

  protected void throwExceptionIfAny() throws JMSException {
    if (null != myException) {
      throw myException;
    }
  }
}
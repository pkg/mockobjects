package com.mockobjects.jms;

import javax.jms.Connection;
import javax.jms.ConnectionMetaData;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import com.mockobjects.ExpectationCounter;
import com.mockobjects.MockObject;

public class MockConnection extends MockObject implements Connection {

    protected ExpectationCounter myCloseCalls = new ExpectationCounter("CommonMockConnection.close");
    protected ExpectationCounter myStartCalls = new ExpectationCounter("CommonMockConnection.start");
    protected ExpectationCounter myStopCalls = new ExpectationCounter("CommonMockConnection.stop");

    private JMSException myException;

    public void close() throws JMSException {
        myCloseCalls.inc();
        throwExceptionIfAny();
    }

    public String getClientID() throws JMSException {
        notImplemented();
        return null;
    }

    public ExceptionListener getExceptionListener() throws JMSException {
        notImplemented();
        return null;
    }

    public ConnectionMetaData getMetaData() throws JMSException {
        notImplemented();
        return null;
    }

    public void setClientID(String clientID) throws JMSException {
        notImplemented();
    }

    public void setExceptionListener(ExceptionListener listener) throws JMSException {
        //does nothing
    }

    public void start() throws JMSException {
        myStartCalls.inc();
        throwExceptionIfAny();
    }

    public void stop() throws JMSException {
        myStopCalls.inc();
        throwExceptionIfAny();
    }

    public void setExpectedCloseCalls(int callCount) {
        myCloseCalls.setExpected(callCount);
    }

    public void setExpectedStartCalls(int callCount) {
        myStartCalls.setExpected(callCount);
    }

    public void setExpectedStopCalls(int callCount) {
        myStopCalls.setExpected(callCount);
    }

    public void setupThrowException(JMSException e) {
        myException = e;
    }

    protected void throwExceptionIfAny() throws JMSException {
        if (null != myException) {
            throw myException;
        }
    }
}

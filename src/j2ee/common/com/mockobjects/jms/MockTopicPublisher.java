package com.mockobjects.jms;

import javax.jms.*;
import com.mockobjects.*;

public class MockTopicPublisher extends MockMessagePublisher
implements TopicPublisher{

    private ExpectationValue message = new ExpectationValue("message");

    public void setExpectedMessage(Message message){
        this.message.setExpected(message);
    }

    public Topic getTopic() throws JMSException{
        notImplemented();
        return null;
    }

    public void publish(Message message) throws JMSException{
        this.message.setActual(message);
    }

    public void publish(Message message, int deliveryMode, int priority,
    long timeToLive) throws JMSException{
        this.message.setActual(message);
    }

    public void publish(Topic topic, Message message) throws JMSException{
        this.message.setActual(message);
    }

    public void publish(Topic topic, Message message, int deliveryMode,
    int priority, long timeToLive) throws JMSException{
        this.message.setActual(message);
    }

}

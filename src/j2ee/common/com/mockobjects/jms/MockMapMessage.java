
package com.mockobjects.jms;

import com.mockobjects.ExpectationMap;
import com.mockobjects.ExpectationSet;
import com.mockobjects.MapEntry;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import java.util.Enumeration;

/**
 * MockMapMessage
 *
 * @author ggalli@e-tree.com
 * @version $Revision: 1.1 $
 */
public class MockMapMessage extends MockMessage implements MapMessage {
    private ExpectationSet myObjectProperties = new ExpectationSet("objectProperty");

    public boolean getBoolean(String aKey) throws JMSException {
        notImplemented();
        return false;
    }

    public byte getByte(String aKey) throws JMSException {
        notImplemented();
        return 0;
    }

    public byte[] getBytes(String aKey) throws JMSException {
        notImplemented();
        return new byte[]{};
    }

    public char getChar(String aKey) throws JMSException {
        notImplemented();
        return 0;
    }

    public double getDouble(String aKey) throws JMSException {
        notImplemented();
        return 0;
    }

    public float getFloat(String aKey) throws JMSException {
        notImplemented();
        return 0;
    }

    public int getInt(String aKey) throws JMSException {
        notImplemented();
        return 0;
    }

    public long getLong(String aKey) throws JMSException {
        notImplemented();
        return 0;
    }

    public Enumeration getMapNames() throws JMSException {
        notImplemented();
        return null;
    }

    public Object getObject(String aKey) throws JMSException {
        notImplemented();
        return null;
    }

    public short getShort(String aKey) throws JMSException {
        notImplemented();
        return 0;
    }

    public String getString(String aKey) throws JMSException {
        notImplemented();
        return null;
    }

    public boolean itemExists(String aKey) throws JMSException {
        notImplemented();
        return false;
    }

    public void setBoolean(String aKey, boolean p) throws JMSException {
        notImplemented();
    }

    public void setByte(String aKey, byte p) throws JMSException {
        notImplemented();
    }

    public void setBytes(String aKey, byte[] p) throws JMSException {
        notImplemented();
    }

    public void setBytes(String aKey, byte[] p, int n, int n1) throws JMSException {
        notImplemented();
    }

    public void setChar(String aKey, char p) throws JMSException {
        notImplemented();
    }

    public void setDouble(String aKey, double p) throws JMSException {
        notImplemented();
    }

    public void setFloat(String aKey, float p) throws JMSException {
        notImplemented();
    }

    public void setInt(String aKey, int p) throws JMSException {
        notImplemented();
    }

    public void setLong(String aKey, long p) throws JMSException {
        notImplemented();
    }

    public void setObject(String aKey, Object p) throws JMSException {
        notImplemented();
    }

    public void setObjectProperty(String aKey, Object anObject) {
        myObjectProperties.addActual(new MapEntry(aKey, anObject));
    }

    public void setShort(String aKey, short p) throws JMSException {
        notImplemented();
    }

    public void setString(String aKey, String aKey1) throws JMSException {
        notImplemented();
    }

    public void setExpectedObjectProperty(String aKey, Object anObject) {
        myObjectProperties.addExpected(new MapEntry(aKey, anObject));
    }
}


package com.mockobjects.jms;

import com.mockobjects.*;
import javax.jms.*;

public class MockMessagePublisher extends MockObject implements MessageProducer{
    public void close() throws JMSException{
        notImplemented();
    }

    public int getDeliveryMode() throws JMSException{
        notImplemented();
        return -1;
    }

    public boolean getDisableMessageID() throws JMSException{
        notImplemented();
        return false;
    }

    public boolean getDisableMessageTimestamp() throws JMSException{
        notImplemented();
        return false;
    }

    public int getPriority() throws JMSException{
        notImplemented();
        return -1;
    }

    public long getTimeToLive() throws JMSException{
        notImplemented();
        return -1;
    }

    public void setDeliveryMode(int deliveryMode) throws JMSException{
        notImplemented();
    }

    public void setDisableMessageID(boolean disableMessageId)
    throws JMSException{
        notImplemented();
    }

    public void setDisableMessageTimestamp(boolean disableMessageTimeStamp)
    throws JMSException{
        notImplemented();
    }

    public void setPriority(int priority) throws JMSException{
        notImplemented();
    }

    public void setTimeToLive(long timeToLive) throws JMSException{
        notImplemented();
    }

}

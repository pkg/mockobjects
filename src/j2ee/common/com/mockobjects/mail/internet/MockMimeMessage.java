package com.mockobjects.mail.internet;

import alt.javax.mail.internet.MimeMessage;
import com.mockobjects.ExpectationList;
import com.mockobjects.mail.MockMessage;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.search.SearchTerm;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

public class MockMimeMessage extends MockMessage implements MimeMessage {
    private final ExpectationList myAddresses =
        new ExpectationList("addresses");
    private final ExpectationList myRecipientTypes =
        new ExpectationList("recipient types");

    public void setRecipient(javax.mail.Message.RecipientType type, Address address) throws MessagingException {
        notImplemented();
    }

    public void setExpectedAddRecipient(javax.mail.Message.RecipientType aType,
        Address anAddress) {

        myAddresses.addExpected(anAddress);
        myRecipientTypes.addExpected(aType);
    }

    public void addRecipient(javax.mail.Message.RecipientType aType,
        Address anAddress) throws MessagingException {
        myAddresses.addActual(anAddress);
        myRecipientTypes.addActual(aType);
    }

    public void setFlag(Flags.Flag flag, boolean b) throws MessagingException {
        notImplemented();
    }

    public int getMessageNumber() {
        notImplemented();
        return 0;
    }

    public String getBody() {
        notImplemented();
        return null;
    }

    public Folder getFolder() {
        notImplemented();
        return null;
    }

    public void setBody(String messageText) {
        notImplemented();
    }

    public boolean isExpunged() {
        notImplemented();
        return false;
    }

    public boolean match(SearchTerm term) throws MessagingException {
        notImplemented();
        return false;
    }

    public Address[] getAllRecipients() throws MessagingException {
        notImplemented();
        return new Address[0];
    }

    public void setRecipients(javax.mail.Message.RecipientType type, String s) throws MessagingException {
        notImplemented();
    }

    public void addRecipients(javax.mail.Message.RecipientType type, String s) throws MessagingException {
        notImplemented();
    }

    public Address[] getReplyTo() throws MessagingException {
        notImplemented();
        return new Address[0];
    }

    public void setReplyTo(Address[] addresses) throws MessagingException {
        notImplemented();
    }

    public void setSubject(String s, String s1) throws MessagingException {
        notImplemented();
    }

    public String getEncoding() throws MessagingException {
        notImplemented();
        return null;
    }

    public String getContentID() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setContentID(String s) throws MessagingException {
        notImplemented();
    }

    public String getContentMD5() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setContentMD5(String s) throws MessagingException {
        notImplemented();
    }

    public void setDescription(String s, String s1) throws MessagingException {
        notImplemented();
    }

    public String[] getContentLanguage() throws MessagingException {
        notImplemented();
        return new String[0];
    }

    public void setContentLanguage(String[] strings) throws MessagingException {
        notImplemented();
    }

    public String getMessageID() throws MessagingException {
        notImplemented();
        return null;
    }

    public InputStream getRawInputStream() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setText(String s, String s1) throws MessagingException {
        notImplemented();
    }

    public void writeTo(OutputStream stream, String[] strings) throws IOException, MessagingException {
        notImplemented();
    }

    public String getHeader(String s, String s1) throws MessagingException {
        notImplemented();
        return null;
    }

    public void addHeaderLine(String s) throws MessagingException {
        notImplemented();
    }

    public Enumeration getAllHeaderLines() throws MessagingException {
        notImplemented();
        return null;
    }

    public Enumeration getMatchingHeaderLines(String[] strings) throws MessagingException {
        notImplemented();
        return null;
    }

    public Enumeration getNonMatchingHeaderLines(String[] strings) throws MessagingException {
        notImplemented();
        return null;
    }

    public boolean isSet(Flags.Flag flag) throws MessagingException {
        notImplemented();
        return false;
    }
}

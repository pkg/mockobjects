package com.mockobjects.mail.internet;

import com.mockobjects.*;
import alt.javax.mail.Session;
import alt.javax.mail.internet.MimeMessageFactory;

import alt.javax.mail.internet.MimeMessage;

public class MockMimeMessageFactory extends MockObject implements
    MimeMessageFactory {

    private final ExpectationValue mySession = new ExpectationValue("session");
    private final ReturnValue myMimeMessage = new ReturnValue("mime message");

    public void setExpectedSession(Session aSession) {
        mySession.setExpected(aSession);
    }

    public void setupCreateMimeMessage(MimeMessage aMimeMessage) {
        myMimeMessage.setValue(aMimeMessage);
    }

    public MimeMessage createMimeMessage(Session aSession) {
        mySession.setActual(aSession);
        return (MimeMessage)myMimeMessage.getValue();
    }

}

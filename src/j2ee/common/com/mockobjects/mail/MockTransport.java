package com.mockobjects.mail;

import javax.mail.event.TransportListener;
import alt.javax.mail.Message;
import javax.mail.Address;
import com.mockobjects.*;
import alt.javax.mail.Transport;

public class MockTransport extends MockService implements Transport {

    private final ExpectationValue myMessage = new ExpectationValue("message");

    public void setExpectedMessage(Message aMessage){
        myMessage.setExpected(aMessage);
    }

    public void send(Message aMessage){
        myMessage.setActual(aMessage);
    }

    public void send(Message msg, Address[] address){
		notImplemented();
    }

    public void sendMessage(Message msg, Address[] address){
		notImplemented();
    }

    public void addTransportListener(TransportListener transportListener){
		notImplemented();
	}

    public void removeTransportListener(TransportListener transportListener){
		notImplemented();
	}

}

package alt.javax.mail;

import java.util.Properties;
import javax.mail.NoSuchProviderException;
import javax.mail.MessagingException;
import javax.mail.Authenticator;
import javax.mail.Provider;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.Folder;
import alt.javax.mail.Transport;
import javax.mail.Address;
import javax.mail.PasswordAuthentication;

public interface Session {
    public Session getInstance(Properties props, Authenticator
        authenticator);
    public Session getInstance(Properties props);
    public Session getDefaultInstance(Properties props, Authenticator
        authenticator);
    public Session getDefaultInstance(Properties props);
    public void setDebug(boolean debug);
    public boolean getDebug();
    public Provider getProviders()[];
    public Provider getProvider(String protocol) throws NoSuchProviderException;
    public void setProvider(Provider provider) throws NoSuchProviderException;
    public Store getStore() throws NoSuchProviderException;
    public Store getStore(String protocol) throws NoSuchProviderException;
    public Store getStore(URLName url) throws NoSuchProviderException;
    public Store getStore(Provider provider) throws NoSuchProviderException;
    public Folder getFolder(URLName url) throws MessagingException;
    public Transport getTransport() throws NoSuchProviderException;
    public Transport getTransport(String protocol) throws NoSuchProviderException;
    public Transport getTransport(URLName url) throws NoSuchProviderException;
    public Transport getTransport(Provider provider) throws NoSuchProviderException;
    public Transport getTransport(Address address) throws NoSuchProviderException;
    public void setPasswordAuthentication(URLName url, PasswordAuthentication pw);
    public PasswordAuthentication getPasswordAuthentication(URLName url);
    public PasswordAuthentication requestPasswordAuthentication(java.net.InetAddress addr, int port, String protocol, String prompt, String defaultUserName);
    public Properties getProperties();
    public String getProperty(String name);
    public javax.mail.Session getWrappedSession();
}

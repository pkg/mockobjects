package com.mockobjects.servlet;

import java.util.*;
import javax.servlet.http.*;
import com.mockobjects.*;

public class MockHttpServletRequest implements HttpServletRequest {
    private Dictionary myParameters = new Hashtable();

    private String myPathInfo;

    public MockHttpServletRequest() {
        super();
    }

    /**
     * @deprecated
     */
    public void addActualParameter(String paramName, String[] values) {
        setupAddParameter(paramName, values);
    }

    /**
     * @deprecated
     */
    public void addActualParameter(String paramName, String value) {
        setupAddParameter(paramName, value);
    }

    public Object getAttribute(String arg1) {
        return null;
    }

    public java.util.Enumeration getAttributeNames() {
        return null;
    }

    public String getAuthType() {
        return null;
    }

    public String getCharacterEncoding() {
        return null;
    }

    public int getContentLength() {
        return 0;
    }

    public String getContentType() {
        return null;
    }

    public java.lang.String getContextPath() {
        return null;
    }

    public javax.servlet.http.Cookie[] getCookies() {
        return null;
    }

    public long getDateHeader(String arg1) {
        return 0;
    }

    public String getHeader(String arg1) {
        return null;
    }

    public java.util.Enumeration getHeaderNames() {
        return null;
    }

    public java.util.Enumeration getHeaders(java.lang.String arg1) {
        return null;
    }

    public javax.servlet.ServletInputStream getInputStream()
        throws java.io.IOException {
        return null;
    }

    public int getIntHeader(String arg1) {
        return 0;
    }

    public java.util.Locale getLocale() {
        return null;
    }

    public java.util.Enumeration getLocales() {
        return null;
    }

    public String getMethod() {
        return null;
    }

    public String getParameter(String paramName) {
        String[] values = getParameterValues(paramName);
        return (values == null ? null : values[0]);
    }

    public java.util.Enumeration getParameterNames() {
        return myParameters.keys();
    }

    public String[] getParameterValues(String key) {
        return (String[]) myParameters.get(key);
    }

    public String getPathInfo() {
        return myPathInfo;
    }

    public String getPathTranslated() {
        return null;
    }

    public String getProtocol() {
        return null;
    }

    public String getQueryString() {
        return null;
    }

    public java.io.BufferedReader getReader() throws java.io.IOException {
        return null;
    }

    public String getRealPath(String arg1) {
        return null;
    }

    public String getRemoteAddr() {
        return null;
    }

    public String getRemoteHost() {
        return null;
    }

    public String getRemoteUser() {
        return null;
    }

    public javax.servlet.RequestDispatcher getRequestDispatcher(
        java.lang.String arg1) {
        return null;
    }

    public String getRequestedSessionId() {
        return null;
    }

    public String getRequestURI() {
        return null;
    }

    public String getScheme() {
        return null;
    }

    public String getServerName() {
        return null;
    }

    public int getServerPort() {
        return 0;
    }

    public String getServletPath() {
        return null;
    }

    public javax.servlet.http.HttpSession getSession() {
        return null;
    }

    public javax.servlet.http.HttpSession getSession(boolean arg1) {
        return null;
    }

    public java.security.Principal getUserPrincipal() {
        return null;
    }

    public boolean isRequestedSessionIdFromCookie() {
        return false;
    }

    public boolean isRequestedSessionIdFromUrl() {
        return false;
    }


    public boolean isRequestedSessionIdFromURL() {
        return false;
    }


    public boolean isRequestedSessionIdValid() {
        return false;
    }

    public boolean isSecure() {
        return false;
    }

    public boolean isUserInRole(java.lang.String arg1) {
        return false;
    }

    public void removeAttribute(java.lang.String arg1) {
    }

    public void setAttribute(String arg1, Object arg2) {
    }

    /**
     * @deprecated
     */
    public void setNoActualParameters() {
        setupNoParameters();
    }

    public void setupAddParameter(String paramName, String[] values) {
        myParameters.put(paramName, values);
    }

    public void setupAddParameter(String paramName, String value) {
        setupAddParameter(paramName, new String[] { value });
    }

    public void setupNoParameters() {
        myParameters = new Hashtable();
    }

    public void setupPathInfo(String pathInfo) {
        myPathInfo = pathInfo;
    }
}

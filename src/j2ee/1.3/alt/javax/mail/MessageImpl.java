package alt.javax.mail;

import com.mockobjects.MockObject;

import javax.mail.MessagingException;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Multipart;
import java.util.Date;
import java.util.Enumeration;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MessageImpl implements Message {
    private final javax.mail.Message message;

    public MessageImpl(javax.mail.Message message) {
        this.message = message;
    }

    public javax.mail.Message getRealMessage() {
        return message;
    }

    public final Address[] getFrom() throws MessagingException {
        return message.getFrom();
    }

    public final void setFrom() throws MessagingException {
        message.setFrom();
    }

    public final void setFrom(Address address) throws MessagingException {
        message.setFrom(address);
    }

    public final void addFrom(Address[] addresses) throws MessagingException {
        message.addFrom(addresses);
    }

    public final Address[] getRecipients(javax.mail.Message.RecipientType type) throws MessagingException {
        return message.getRecipients(type);
    }

    public final void setRecipients(javax.mail.Message.RecipientType type, Address[] addresses) throws MessagingException {
        message.setRecipients(type, addresses);
    }

    public final void addRecipients(javax.mail.Message.RecipientType type, Address[] addresses) throws MessagingException {
        message.addRecipients(type, addresses);
    }

    public final String getSubject() throws MessagingException {
        return message.getSubject();
    }

    public final void setSubject(String s) throws MessagingException {
        message.setSubject(s);
    }

    public final Date getSentDate() throws MessagingException {
        return message.getSentDate();
    }

    public final void setSentDate(Date date) throws MessagingException {
        message.setSentDate(date);
    }

    public final Date getReceivedDate() throws MessagingException {
        return message.getReceivedDate();
    }

    public final Flags getFlags() throws MessagingException {
        return message.getFlags();
    }

    public final void setFlags(Flags flags, boolean b) throws MessagingException {
        message.setFlags(flags, b);
    }

    public final Message reply(boolean b) throws MessagingException {
        return new MessageImpl(message.reply(b));
    }

    public final void saveChanges() throws MessagingException {
        message.saveChanges();
    }

    public final int getSize() throws MessagingException {
        return message.getSize();
    }

    public final int getLineCount() throws MessagingException {
        return message.getLineCount();
    }

    public final String getContentType() throws MessagingException {
        return message.getContentType();
    }

    public final boolean isMimeType(String s) throws MessagingException {
        return message.isMimeType(s);
    }

    public final String getDisposition() throws MessagingException {
        return message.getDisposition();
    }

    public final void setDisposition(String s) throws MessagingException {
        message.setDisposition(s);
    }

    public final String getDescription() throws MessagingException {
        return message.getDescription();
    }

    public final void setDescription(String s) throws MessagingException {
        message.setDescription(s);
    }

    public final String getFileName() throws MessagingException {
        return message.getFileName();
    }

    public final void setFileName(String s) throws MessagingException {
        message.setFileName(s);
    }

    public final InputStream getInputStream() throws IOException, MessagingException {
        return message.getInputStream();
    }

    public final javax.activation.DataHandler getDataHandler() throws MessagingException {
        return message.getDataHandler();
    }

    public final Object getContent() throws IOException, MessagingException {
        return message.getContent();
    }

    public final void setDataHandler(javax.activation.DataHandler handler) throws MessagingException {
        message.setDataHandler(handler);
    }

    public final void setContent(Object o, String s) throws MessagingException {
        message.setContent(o, s);
    }

    public final void setText(String s) throws MessagingException {
        message.setText(s);
    }

    public final void setContent(Multipart multipart) throws MessagingException {
        message.setContent(multipart);
    }

    public final void writeTo(OutputStream stream) throws IOException, MessagingException {
        message.writeTo(stream);
    }

    public final String[] getHeader(String s) throws MessagingException {
        return message.getHeader(s);
    }

    public final void setHeader(String s, String s1) throws MessagingException {
        message.setHeader(s, s1);
    }

    public final void addHeader(String s, String s1) throws MessagingException {
        message.addHeader(s, s1);
    }

    public final void removeHeader(String s) throws MessagingException {
        message.removeHeader(s);
    }

    public final Enumeration getAllHeaders() throws MessagingException {
        return message.getAllHeaders();
    }

    public final Enumeration getMatchingHeaders(String[] strings) throws MessagingException {
        return message.getAllHeaders();
    }

    public final Enumeration getNonMatchingHeaders(String[] strings) throws MessagingException {
        return message.getNonMatchingHeaders(strings);
    }
}

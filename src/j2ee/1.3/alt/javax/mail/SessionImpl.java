package alt.javax.mail;

import javax.mail.*;
import java.util.Properties;

public class SessionImpl implements Session {
    private final javax.mail.Session session;

    public SessionImpl(javax.mail.Session session) {
        this.session = session;
    }

    public Session getInstance(Properties props, Authenticator
        authenticator) {

        return new SessionImpl(session.getInstance(props, authenticator));
    }

    public Session getInstance(Properties props) {
        return new SessionImpl(session.getInstance(props));
    }

    public Session getDefaultInstance(Properties props, Authenticator
        authenticator) {
        return new SessionImpl(session.getDefaultInstance(
            props, authenticator));
    }

    public Session getDefaultInstance(Properties props) {
        return new SessionImpl(session.getDefaultInstance(props));
    }

    public void setDebug(boolean debug) {
    }

    public boolean getDebug() {
        return session.getDebug();
    }

    public Provider getProviders()[] {
        return session.getProviders();
    }

    public Provider getProvider(String protocol) throws NoSuchProviderException {
        return session.getProvider(protocol);
    }

    public void setProvider(Provider provider) throws NoSuchProviderException {
        session.setProvider(provider);
    }

    public Store getStore() throws NoSuchProviderException {
        return session.getStore();
    }

    public Store getStore(String protocol) throws NoSuchProviderException {
        return session.getStore(protocol);
    }

    public Store getStore(URLName url) throws NoSuchProviderException {
        return session.getStore(url);
    }

    public Store getStore(Provider provider) throws NoSuchProviderException {
        return session.getStore(provider);
    }

    public Folder getFolder(URLName url) throws MessagingException {
        return session.getFolder(url);
    }

    public Transport getTransport() throws NoSuchProviderException {
        return new TransportImpl(session.getTransport());
    }

    public Transport getTransport(String protocol) throws NoSuchProviderException {
        return new TransportImpl(session.getTransport(protocol));
    }

    public Transport getTransport(URLName url) throws NoSuchProviderException {
        return new TransportImpl(session.getTransport(url));
    }

    public Transport getTransport(Provider provider) throws NoSuchProviderException {
        return new TransportImpl(session.getTransport(provider));
    }

    public Transport getTransport(Address address) throws NoSuchProviderException {
        return new TransportImpl(session.getTransport(address));
    }

    public void setPasswordAuthentication(URLName url, PasswordAuthentication pw) {
        session.setPasswordAuthentication(url, pw);
    }

    public PasswordAuthentication getPasswordAuthentication(URLName url) {
        return session.getPasswordAuthentication(url);
    }

    public PasswordAuthentication requestPasswordAuthentication(java.net.InetAddress addr, int port, String protocol, String prompt, String defaultUserName) {
        return session.requestPasswordAuthentication(addr, port, protocol,
            prompt, defaultUserName);
    }

    public Properties getProperties() {
        return session.getProperties();
    }

    public String getProperty(String name) {
        return session.getProperty(name);
    }

    public javax.mail.Session getWrappedSession() {
        return session;
    }

}

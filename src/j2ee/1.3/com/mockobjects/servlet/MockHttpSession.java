package com.mockobjects.servlet;

import com.mockobjects.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.Enumeration;

public class MockHttpSession extends MockObject implements HttpSession, Verifiable {
    private ExpectationSet myAttributes = new ExpectationSet("session attributes");
    private ExpectationSet myRemovedAttributes = new ExpectationSet("removed session attributes");
    private ReturnObjectBag myAttributeValues = new ReturnObjectBag("attributes");
    private Enumeration attributeNames;

    private ServletContext servletContext;

    public Object getAttribute(String anAttributeName) {
        return myAttributeValues.getNextReturnObject(anAttributeName);
    }

    public void setupGetAttributeNames(Enumeration attributeNames) {
        this.attributeNames = attributeNames;
    }

    public Enumeration getAttributeNames() {
        return attributeNames;
    }

    public long getCreationTime() {
        notImplemented();
        return 0;
    }

    public String getId() {
        notImplemented();
        return null;
    }

    public long getLastAccessedTime() {
        notImplemented();
        return 0;
    }

    public int getMaxInactiveInterval() {
        notImplemented();
        return 0;
    }

    public HttpSessionContext getSessionContext() {
        notImplemented();
        return null;
    }

    public void setupServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public Object getValue(String arg1) {
        notImplemented();
        return null;
    }

    public String[] getValueNames() {
        notImplemented();
        return null;
    }

    public void invalidate() {
        notImplemented();
    }

    public boolean isNew() {
        notImplemented();
        return false;
    }

    public void putValue(String arg1, Object arg2) {
        notImplemented();
    }

    public void setExpectedRemoveAttribute(String anAttributeName){
        myRemovedAttributes.addExpected(anAttributeName);
    }

    public void removeAttribute(String anAttributeName) {
        myRemovedAttributes.addActual(anAttributeName);
    }

    public void removeValue(String arg1) {
        notImplemented();
    }

    public void setupGetAttribute(String key, Object value){
        myAttributeValues.putObjectToReturn(key, value);
    }

    public void setAttribute(String aKey, Object aValue) {
        myAttributes.addActual(new MapEntry(aKey, aValue));
    }

    public void setExpectedAttribute(String aKey, Object aValue) {
        myAttributes.addExpected(new MapEntry(aKey, aValue));
    }

    public void setMaxInactiveInterval(int arg1) {
        notImplemented();
    }
}

package com.mockobjects.helpers;

import com.mockobjects.servlet.MockFilterChain;
import com.mockobjects.servlet.MockFilterConfig;

import javax.servlet.Filter;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * $Revision: 1.1 $
 */
public class FilterTestHelper extends AbstractServletTestHelper {
    private final Filter filter;
    private final MockFilterChain filterChain = new MockFilterChain();
    private final MockFilterConfig filterConfig = new MockFilterConfig();

    public FilterTestHelper(Filter filter) {
        this.filter = filter;
        filterConfig.setupGetServletContext(servletContext);
    }

    public MockFilterChain getFilterChain() {
        return filterChain;
    }

    public MockFilterConfig getFilterConfig() {
        return filterConfig;
    }

    public void testInit() throws ServletException {
        filter.init(filterConfig);
    }

    public void testDoFilter() throws ServletException, IOException {
        filter.doFilter(request, response, filterChain);
    }
}

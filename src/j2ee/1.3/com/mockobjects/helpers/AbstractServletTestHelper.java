package com.mockobjects.helpers;

import com.mockobjects.servlet.*;

/**
 * $Revision: 1.1 $
 */
public abstract class AbstractServletTestHelper {
    protected final MockHttpServletRequest request = new MockHttpServletRequest();
    protected final MockHttpServletResponse response = new MockHttpServletResponse();
    protected final MockHttpSession httpSession = new MockHttpSession();
    protected final MockRequestDispatcher requestDispatcher = new MockRequestDispatcher();
    protected final MockServletContext servletContext = new MockServletContext();
    protected final MockServletConfig servletConfig = new MockServletConfig();

    public AbstractServletTestHelper() {
        request.setSession(httpSession);
        servletContext.setupGetRequestDispatcher(requestDispatcher);
        request.setupGetRequestDispatcher(requestDispatcher);
        httpSession.setupServletContext(servletContext);
        servletConfig.setServletContext(servletContext);
    }

    public MockHttpServletRequest getRequest() {
        return request;
    }

    public MockHttpSession getHttpSession() {
        return httpSession;
    }

    public MockRequestDispatcher getRequestDispatcher() {
        return requestDispatcher;
    }

    public MockHttpServletResponse getResponse() {
        return response;
    }

    public MockServletContext getServletContext() {
        return servletContext;
    }

    public MockServletConfig getServletConfig() {
        return servletConfig;
    }
}

package com.mockobjects.sql;

/**
 * Empty implementation of <code>CommonMockDataSource</code>.
 * Implementations for later releases of Java (e.g. 1.4)
 * may include additional members.
 * @see <a href="http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/sql/DataSource.html">javax.sql.DataSource</a>
 * @author Ted Husted
 * @version $Revision: 1.1 $ $Date: 2002/08/27 16:34:03 $
 */
public class MockDataSource extends CommonMockDataSource{
}

package com.mockobjects.examples.calcserver;

import com.mockobjects.util.TestCaseMo;

/**
 * JUnit test case for CalculatorImplTest
 */

public class TestCalculator extends TestCaseMo {
    private static final Class THIS = TestCalculator.class;

    Calculator myCalculator = new Calculator();


    public TestCalculator(String name) {
        super(name);
    }


    public static void main(String[] args) {
        start(new String[]{ THIS.getName()});
    }


    public void testAdd() throws CalculatorException {
        assertEquals("add", 9, myCalculator.calculate(5, 4, "/add"));
    }


    public void testBadOperation() {
        try {
            myCalculator.calculate(5, 4, "Bad operation");
            fail("Should have thrown an exception");
        } catch (CalculatorException expected) {
        }
    }


    public void testSubtract() throws CalculatorException {
        assertEquals("subtract", 1, myCalculator.calculate(5, 4, "/subtract"));
    }
}
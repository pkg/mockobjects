package com.mockobjects.examples.calcserver;

import junit.framework.Test;
import junit.framework.TestSuite;
import com.mockobjects.util.SuiteBuilder;
import com.mockobjects.util.TestCaseMo;

public class AllTests extends TestCaseMo {
    private static final Class THIS = AllTests.class;

    public AllTests(String name) {
        super(name);
    }


    public static void addTestCalculator(TestSuite suite) {
        suite.addTestSuite(TestCalculator.class);
    }


    public static void addTestCalculatorServlet(TestSuite suite) {
        suite.addTestSuite(TestCalculatorServlet.class);
    }


    public static void addTestSavingCalculator(TestSuite suite) {
        suite.addTestSuite(TestSavingCalculator.class);
    }


    public static void main(String[] args) {
        start(new String[]{ THIS.getName()});
    }


    public static Test suite() {
        return SuiteBuilder.buildTest(THIS);
    }
}
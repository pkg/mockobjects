package com.mockobjects.examples.calcserver;

import java.sql.SQLException;
import com.mockobjects.sql.MockPreparedStatement;
import com.mockobjects.util.TestCaseMo;

public class TestSavingCalculator extends TestCaseMo {
    private static final Class THIS = TestSavingCalculator.class;


    public TestSavingCalculator(String name) {
        super(name);
    }


    public static void main(String[] args) {
        start(new String[]{ THIS.getName()});
    }


    public void testSave() throws CalculatorException {
        MockCalculator myMockCalculator = new MockCalculator();
        MockPreparedStatement myMockStatement = new MockPreparedStatement();

        SqlSavingCalculator mySavingCalculator =
                new SqlSavingCalculator(myMockCalculator, myMockStatement);
        myMockCalculator.setupResult(666);

        myMockCalculator.setExpectedCalculation(5, 3, "/add");
        myMockStatement.addExpectedSetParameter(1, 666);
        myMockStatement.setExpectedExecuteCalls(1);

        mySavingCalculator.calculate(5, 3, "/add");

        myMockCalculator.verify();
        myMockStatement.verify();
    }


    public void testSqlFailure() throws CalculatorException {
        MockCalculator myMockCalculator = new MockCalculator();
        MockPreparedStatement myMockStatement = new MockPreparedStatement();

        SqlSavingCalculator mySavingCalculator =
                new SqlSavingCalculator(myMockCalculator, myMockStatement);

        myMockCalculator.setExpectedCalculation(5, 3, "/add");
        myMockStatement.setExpectedExecuteCalls(1);
        myMockStatement.setupThrowExceptionOnExecute(new SQLException("Mock exception"));

        try {
            mySavingCalculator.calculate(5, 3, "/add");
            fail("Should throw exception");
        } catch (CalculatorException expected) {
            //
        }

        myMockCalculator.verify();
        myMockStatement.verify();
    }
}
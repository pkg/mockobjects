package com.mockobjects.examples.calcserver;

import com.mockobjects.ExpectationValue;
import com.mockobjects.MockObject;

public class MockCalculator extends MockObject implements IntCalculator {
    private int myResult;
    private ExpectationValue myValue1 = new ExpectationValue("value1");
    private ExpectationValue myValue2 = new ExpectationValue("value2");
    private ExpectationValue myOperation = new ExpectationValue("operation");
    private String myBadOperation;


    /**
     * MockCalculator constructor comment.
     */
    public MockCalculator() {
        super();
    }


    public int calculate(int value1, int value2, String operation) throws CalculatorException {
        myValue1.setActual(value1);
        myValue2.setActual(value2);
        myOperation.setActual(operation);
        return myResult;
    }


    public void setExpectedCalculation(int value1, int value2, String operation) {
        myValue1.setExpected(value1);
        myValue2.setExpected(value2);
        myOperation.setExpected(operation);
    }


    public void setupResult(int result) {
        myResult = result;
    }


    public void setupThrowBadOperation(String opName) {
        myBadOperation = opName;
    }


    public void verify() {
        myValue1.verify();
        myValue2.verify();
        myOperation.verify();
    }
}
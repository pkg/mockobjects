package com.mockobjects.examples.calcserver;

public class Calculator implements IntCalculator {

    /**
     * Calculator constructor comment.
     */
    public Calculator() {
        super();
    }


    public int calculate(int value1, int value2, String operation) throws CalculatorException {
        if ("/add".equalsIgnoreCase(operation)) {
            return value1 + value2;
        } else if ("/subtract".equalsIgnoreCase(operation)) {
            return value1 - value2;
        } else {
            throw new CalculatorException("Bad operation: " + operation);
        }
    }
}
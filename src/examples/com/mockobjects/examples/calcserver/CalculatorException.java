package com.mockobjects.examples.calcserver;

public class CalculatorException extends Exception {

    /**
     * CalculatorException constructor comment.
     */
    public CalculatorException() {
        super();
    }


    /**
     * CalculatorException constructor comment.
     * @param arg1 java.lang.String
     */
    public CalculatorException(String arg1) {
        super(arg1);
    }


    /**
     * CalculatorException constructor comment.
     * @param arg1 java.lang.String
     * @param arg2 java.lang.Throwable
     */
    public CalculatorException(String msg, Throwable ex) {
        this(msg + ex.toString());
    }


    /**
     * CalculatorException constructor comment.
     * @param arg1 java.lang.Throwable
     */
    public CalculatorException(Throwable ex) {
        this(ex.toString());
    }
}
package com.mockobjects.examples.mailinglist;

public interface ListAction {
    public void applyTo(String email, String name) throws MailingListException;
}

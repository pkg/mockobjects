package com.mockobjects.examples.password;

public interface PasswordReminder {
    public void sendReminder(String emailAddress) throws NotFoundException;
}

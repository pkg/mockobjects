package com.mockobjects.examples.password;

import com.mockobjects.ExpectationValue;
import com.mockobjects.MockObject;

public class MockPasswordReminder extends MockObject implements PasswordReminder {
    private ExpectationValue emailAddress = new ExpectationValue("MockPasswordReminder email");
    private boolean emailNotFound = false;

    public void sendReminder(String anEmailAddress) throws NotFoundException {
        emailAddress.setActual(anEmailAddress);
        if (emailNotFound) {
            throw new NotFoundException("Not found: "  + anEmailAddress);
        }
    }

    public void setExpectedEmailAddress(String anEmailAddress) {
        emailAddress.setExpected(anEmailAddress);
    }

    public void setupEmailNotFound() {
        emailNotFound = true;
    }
}

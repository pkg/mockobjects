package com.mockobjects.examples.password;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import com.mockobjects.servlet.MockHttpServletRequest;
import com.mockobjects.servlet.MockHttpServletResponse;
import com.mockobjects.util.TestCaseMo;

public class TestForgotPasswordServlet extends TestCaseMo {
    public static final String SENT_URI = "sent_uri";
    public static final String EMAIL_FAILED_URI = "bad_email_uri";
    public static final String EMAIL_NOT_FOUND_URI = "no_email_uri";
    public static final String EMAIL = "email@an.address";

    public TestForgotPasswordServlet(String name) {
        super(name);
    }

    private MockHttpServletRequest mockRequest = new MockHttpServletRequest();
    private MockHttpServletResponse mockResponse = new MockHttpServletResponse();
    private MockPasswordReminder mockReminder = new MockPasswordReminder();
    private ForgotPasswordServlet passwordServlet;

    public void setUp() throws ServletException, IOException {
        passwordServlet = new ForgotPasswordServlet(mockReminder);

        mockRequest.setupAddParameter(ForgotPasswordServlet.EMAIL_PARAM, EMAIL);
    }

    public void testReminderEmailSent() throws ServletException, IOException {
        mockReminder.setExpectedEmailAddress(EMAIL);
        mockResponse.setExpectedRedirect(
                SENT_URI + "?" + ForgotPasswordServlet.EMAIL_PARAM + "=" + EMAIL);

        passwordServlet.doGet(mockRequest, mockResponse);

        mockResponse.verify();
        mockReminder.verify();
    }

/*
    public void testEmailNotFound() throws ServletException, IOException {
        mockReminder.setupEmailNotFound();

        mockReminder.setExpectedEmailAddress(EMAIL);
        mockResponse.setExpectedRedirect(EMAIL_NOT_FOUND_URI + "?email=" + EMAIL);

        passwordServlet.doGet(mockRequest, mockResponse);

        mockResponse.verify();
        mockReminder.verify();
    }

    private ServletConfig createStubServletConfig() {
        return new StubServletConfig() {
            public String getInitParameter(String s) {
                if (ForgotPasswordServlet.SENT_PARAM_NAME.equals(s)) {
                    return SENT_URI;
                } else if (ForgotPasswordServlet.EMAIL_FAILED_PARAM_NAME.equals(s)) {
                    return EMAIL_FAILED_URI;
                } else if (ForgotPasswordServlet.EMAIL_NOT_FOUND_PARAM_NAME.equals(s)) {
                    return EMAIL_NOT_FOUND_URI;
                }
                return super.getInitParameter(s);
            }
        };
    }*/
}


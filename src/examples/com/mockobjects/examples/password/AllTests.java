package com.mockobjects.examples.password;

import junit.framework.Test;
import junit.framework.TestSuite;
import com.mockobjects.util.SuiteBuilder;
import com.mockobjects.util.TestCaseMo;

public class AllTests extends TestCaseMo {
    private static final Class THIS = AllTests.class;

    public AllTests(String name) {
        super(name);
    }


    public static void addForgotPasswordServlet(TestSuite suite) {
        suite.addTestSuite(TestForgotPasswordServlet.class);
    }


    public static void main(String[] args) {
        start(new String[]{ THIS.getName()});
    }


    public static Test suite() {
        return SuiteBuilder.buildTest(THIS);
    }
}
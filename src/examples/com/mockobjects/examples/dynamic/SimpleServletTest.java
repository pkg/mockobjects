package com.mockobjects.examples.dynamic;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.dynamic.OrderedMock;

/**
 * A sample test case showing usage of the dynamic mock library.
 * It has also been used as an acceptance test for the implementation
 * of the library, and so uses different variations of some of the calls
 */

public class SimpleServletTest extends TestCase {
	final String SUBJECT = "Mail Subject";
	final String[] RECIPIENTS = new String[]{ "tim@hotmail.com", "steve@yahoo.com"};
	final String BODY = "Mail Body";
	
	public SimpleServletTest(String name) {
		super(name);
	}
/*
	public void testDoGetOldStyle() throws ServletException, IOException {
		Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);
		Mock mockHttpServletRequest = new Mock(HttpServletRequest.class);
		
		CallSequence params = new CallSequence();
		params.expectAndReturn( new Constraint[] { C.eq("body") }, "this is the body" );
		params.expectAndReturn( new Constraint[] { C.eq("subject") }, "mail from tim" ); 
		params.expectAndReturn( new Constraint[] { C.eq("recipients") }, new String[] { "nat.pryce@b13media.com", "steve@m3p.com" } );
		
		mockHttpServletRequest.expect( "getParameter", params );
		
		SimpleServlet aServlet = new SimpleServlet();
		aServlet.doGet((HttpServletRequest) mockHttpServletRequest.proxy(), (HttpServletResponse) mockHttpServletResponse.proxy());
		
		mockHttpServletRequest.verify();
		mockHttpServletResponse.verify();
	}
*/	
	public void testDoGet() throws ServletException, IOException {
      
		Mock mockHttpServletRequest = new Mock(HttpServletRequest.class);
		Mock mockHttpServletResponse = new OrderedMock(HttpServletResponse.class, "Response with non-default name");
		Mock mockTimer = new Mock(Timer.class);
		Mock mockMailSender = new Mock(MailSender.class);
		
		// Set no expectations but answer a value for the specified method 
		mockHttpServletRequest.matchAndReturn( "getParameter", C.eq("browser-identifier"), "MSIE-5.0" );
		mockHttpServletRequest.matchAndReturn("getIntHeader", C.ANY_ARGS, 20);	
		
		mockHttpServletRequest.expectAndReturn( "getParameter", "subject", SUBJECT );
		mockHttpServletRequest.expectAndReturn("getParameterValues", "recipients", RECIPIENTS);
		mockHttpServletRequest.expectAndReturn( "getParameter", C.args(C.eq("body")), BODY );
        mockTimer.expectAndReturn("getTime", 10000);
        mockTimer.expectAndReturn("getTime", 20000);
        mockMailSender.expectAndReturn("sendMail", C.eq(SUBJECT, RECIPIENTS, BODY),true);
		
		final PrintWriter contentWriter = new PrintWriter(new StringWriter());
		
		mockHttpServletResponse.expect( "setContentType", "text/html");
		mockHttpServletResponse.expectAndReturn( "getWriter", contentWriter );

// Proposed enhancement to allow individual ordering 
// 
//		CallSignature m1 = mockHttpServletResponse.expect( "setContentType", "text/html");
//		CallSignature m2 = mockHttpServletResponse.expectAndReturn( "getWriter", C.args(), contentWriter );
//		m1.expectBefore(m2);
		
		SimpleServlet aServlet = new SimpleServlet((Timer)mockTimer.proxy(), (MailSender)mockMailSender.proxy());
		aServlet.doGet((HttpServletRequest) mockHttpServletRequest.proxy(), (HttpServletResponse) mockHttpServletResponse.proxy());
		
		mockHttpServletRequest.verify();
		mockHttpServletResponse.verify();
		mockTimer.verify();
		mockMailSender.verify();
	}
}

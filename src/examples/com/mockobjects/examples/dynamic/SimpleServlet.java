package com.mockobjects.examples.dynamic;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @author dev
 */

public class SimpleServlet extends HttpServlet {

    private Timer timer;
    private MailSender mailSender;

    public SimpleServlet(Timer aTimer, MailSender aMailSender) {
        super();
        timer = aTimer;         
        mailSender = aMailSender;
    }
    
    public SimpleServlet() {
         this(null,null);  // really this would be a proper timer and mail sender...       
     }

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String body = request.getParameter("body");
		String subject = request.getParameter("subject");
		String[] recipients = request.getParameterValues("recipients");
		String browser = request.getParameter("browser-identifier");
		
		// Cause a failure by calling something unexpected
		//String pet = request.getParameter("favourite-pet");
		
        int age = request.getIntHeader("age");
        response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
        
        writer.print("timer before:" + timer.getTime());
        mailSender.sendMail(subject, recipients, body);
        writer.print("timer after:" + timer.getTime());
		
	}

}


package com.mockobjects.examples.dynamic;


public interface Timer {
    
    int getTime();

}

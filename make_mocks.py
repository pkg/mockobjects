#!/usr/bin/python2

import os, shutil

libdir = 'lib'
junitlib = libdir+'/junit.jar'
antcmd = 'ant'

if os.environ.has_key('ANT_HOME'):
	anthome=os.environ('ANT_HOME')
else:
	anthome='/opt/ant'

def ante(target, javahome, options):
	ENV={
		'PATH': javahome + '/bin:' + anthome + '/bin:' + os.environ['PATH'],
		'JAVA_HOME': javahome
	}
	target = antcmd+options+' ' + target 
	print '###############################################'
	print ENV
	print '###############################################'
	os.spawnvpe(os.P_WAIT, antcmd, target.split(), ENV)
	print '###############################################'

def ant(target):
	ante(target, os.environ['JAVA_HOME'], '')

def build(env):
	args = env.strip().split(':')
	ante(args[0], args[1], args[2])


ant('clean')

for env in file('env.txt').readlines():
	build(env)

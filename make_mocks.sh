#!/usr/bin/bash
# helper script for building all the combinations
# copy it somewhere it won't be overwritten and set the paths 
# to those on your machine
# This version setup for cygwin on my machine
# - smgf


export ANT_HOME="D:\java\ant"

cd mockobjects-java

rm -f lib/junit.jar
cp /cygdrive/d/java/junit/3.7/junit.jar lib

export PATH=$ANT_HOME/bin:$PATH
ant.bat clean

(
export JAVA_HOME="D:\java\jdk\1.3.1_05"
export PATH=$JAVA_HOME/bin:$PATH

echo "+++++++++ JAVA 1.3 +++++++++++++"

echo "  +++++ JDK 1.2 +++++"
rm -f lib/j2ee.jar
cp /cygdrive/d/java/j2sdkee1.2.1//lib/j2ee.jar lib
ant.bat clean-compiled jar

echo "  +++++ JDK 1.3 +++++"
rm -f lib/j2ee.jar
cp /cygdrive/d/java/j2sdkee1.3.1/lib/j2ee.jar lib
ant.bat clean-compiled jar
)

(
export JAVA_HOME="D:\java\jdk\1.4.1"
export PATH=$JAVA_HOME/bin:$PATH

echo "+++++++++ JAVA 1.4 +++++++++++++"

echo "  +++++ JDK 1.2 +++++"
rm -f lib/j2ee.jar
cp /cygdrive/d/java/j2sdkee1.2.1//lib/j2ee.jar lib
ant.bat clean-compiled jar

echo "  +++++ JDK 1.3 +++++"
rm -f lib/j2ee.jar
cp /cygdrive/d/java/j2sdkee1.3.1/lib/j2ee.jar lib
ant.bat clean-compiled jar
)

